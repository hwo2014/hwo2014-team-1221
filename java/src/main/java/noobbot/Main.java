package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.UnknownHostException;

import noobbot.ai.Driver;
import noobbot.ai.MyRaceCar;
import noobbot.ai.RaceCars;
import noobbot.builders.JsonMsgBuilder;
import noobbot.gui.HelloWorldOpenFrame;
import noobbot.hwo.crash.Crash;
import noobbot.hwo.createrace.CreateRace;
import noobbot.hwo.dnf.Dnf;
import noobbot.hwo.finish.Finish;
import noobbot.hwo.joinrace.JoinRace;
import noobbot.hwo.lapfinished.LapFinished;
import noobbot.hwo.spawn.Spawn;
import noobbot.hwo.throttle.Throttle;
import noobbot.hwo.turbo.Turbo;
import noobbot.hwo.turboavailable.TurboAvailable;
import noobbot.hwo.turboend.TurboEnd;
import noobbot.hwo.turbostart.TurboStart;
import noobbot.hwo.yourcar.YourCar;
import noobbot.logging.DashCauge;
import noobbot.logging.LogLevel;
import noobbot.logging.Logger;
import noobbot.logging.RaceInfo;
import noobbot.wrappers.CarDimensionWrapper;
import noobbot.wrappers.CarPositionsWrapper;
import noobbot.wrappers.CrashWrapper;
import noobbot.wrappers.CurveType;
import noobbot.wrappers.GameEndWrapper;
import noobbot.wrappers.GameInitWrapper;
import noobbot.wrappers.PieceWrapper;
import noobbot.wrappers.TrackWrapper;

import com.google.gson.Gson;

public class Main {

  private static final boolean logOnlyToConsole = true;
  private static String botName;
  private static String botKey;
  private static int raceNameIndex = 0;

  public static void main(String... args) throws Exception {

    String host = args[0];
    int port = Integer.parseInt(args[1]);
    botName = args[2];
    botKey = args[3];

    if (args.length == 5)
    {
      // Use GUI
      HelloWorldOpenFrame.openFrame();
    }
    else
    {
      // CI
      Main main = connectToTestServer(host, port);
      main.startRace(host, logOnlyToConsole);
    }
  }

  public static Main connectToTestServer(String host, int port) throws UnknownHostException, Exception, UnsupportedEncodingException {
    System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

    socket = new Socket(host, port);

    final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

    final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

    Main main = new Main(reader, writer);
    return main;
  }

  private final Gson gson = new Gson();
  private BufferedReader reader;
  private PrintWriter writer;
  private Logger logger = Logger.getInstance();
  public GameInitWrapper gameInit = GameInitWrapper.getInstance();
  public CarPositionsWrapper carPositions;
  public CrashWrapper crashes = new CrashWrapper();

  public MyRaceCar myRaceCar = new MyRaceCar();
  private Driver driver = new Driver(this);

  public HelloWorldOpenFrame gui;
  private static Socket socket;

  public GameEndWrapper gameEnd = GameEndWrapper.getInstance();
  public LapFinished lapFinished;
  public Spawn spawn;
  public Finish finish;
  public Dnf dnf;
  public TurboAvailable turboAvailable;
  public YourCar yourCar;
  public String raceName = "Ruukki";
  private RaceInfo raceInfo;

  private RaceCars raceCars = new RaceCars();
  private TurboStart turbostart;
  private TurboEnd turboend;

  public Main(final BufferedReader reader, final PrintWriter writer) throws Exception {
    this.reader = reader;
    this.writer = writer;
    logger.setMyRaceCar(myRaceCar);
    logger.setMain(this);

  }

  public void startRace(String host, boolean logonlytoconsole) {



    try {
      raceInfo = new RaceInfo(host, "Finland", 1, raceName, false, null, null);
      logger.setRaceInfo(raceInfo);
      logger.setLoggingMode(logonlytoconsole);

      logger.log(LogLevel.PROGRESS, "Sent join msg", raceInfo);
      send(new Join(botName, botKey));
      runRace(reader);
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

  }

  /**
   * not used. joinRace is sufficient
   * 
   * @param carCount
   */
  public void createRace(Integer carCount) {

    try {
      System.out.println("send createRace");
      raceName = botName;
      CreateRace msg = JsonMsgBuilder.getCreateRaceMsg(carCount, botKey, raceName);
      String json = gson.toJson(msg);
      sendJson(json);
      runRace(reader);
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

  }

  /**
   * Used for starting a race on a selected track. Creates the race if it does not exist.
   * This is not used in the official race
   * 
   * @param raceInfo
   */
  public void joinRace(RaceInfo raceInfo) {

    try {
      this.raceInfo = raceInfo;
      logger.setRaceInfo(raceInfo);

      if (raceInfo.raceName != null)
        raceName = raceInfo.raceName;
      else
        raceName = botName;
      if (raceNameIndex > 0)
      {
        raceName = botName + raceNameIndex;
      }

      logger.log(LogLevel.PROGRESS, "send joinRace " + raceName, raceInfo);

      raceNameIndex++;

      JoinRace msg = JsonMsgBuilder.getJoinRaceMsg(raceInfo.numberOfCars, raceInfo.TrackName, raceInfo.password, botKey, raceName);
      String json = gson.toJson(msg);
      sendJson(json);

      int retries = 1000;
      while (true)
      {
        // Added while loop to retry reading if fails
        try {
          runRace(reader);
          return;
        } catch (IOException e) {

          e.printStackTrace();
          retries--;

          if (retries == 0)
            return; // Give up
        }
      }
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  /**
   * This is the main loop for reading messages from the server
   * 
   * @param reader
   * @throws IOException
   */
  private void runRace(final BufferedReader reader) throws Exception {
    String line;
    logger.log(LogLevel.MESSAGE, "\nWaiting for json messages", raceInfo);
    logger.log(LogLevel.PROGRESS, "\nWaiting for json messages", raceInfo);

    while ((line = reader.readLine()) != null) {

      if (gui != null)
      {
        if (gui.shouldStopRace)
          break;
      }
      try {

        final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);

        if (msgFromServer.msgType.equals("carPositions")) {

          carPositions = new CarPositionsWrapper(line, gameInit, raceName);
          raceCars.updateRaceCarPositions(carPositions);
          myRaceCar.setCarInfo(carPositions.getMyCar(), carPositions);

          logger.log(LogLevel.CARPOS_MESSAGE, line, raceInfo);

          doCarPosTick();

        }
        else if (msgFromServer.msgType.equals("join")) {
          logger.log(LogLevel.PROGRESS, "Join (Finland)", raceInfo);
          logger.log(LogLevel.MESSAGE, line, raceInfo);
          logger.log(LogLevel.DETAIL, line, raceInfo);
        }
        else if (msgFromServer.msgType.equals("gameInit")) {
          gameInit.fromJson(line);
          raceCars.setTrackInfo(gameInit.getTrack(), getRaceInfo());
          myRaceCar.setTrackInfo(gameInit.getTrack());

          if (raceInfo.crippled)
          {
            gameInit.getTrack().physics.adjustFrictionValue(raceInfo.crippledCoeficient);
            // gameInit.getTrack().physics.adjustMaxCentrifugalF(raceInfo.crippledCoeficient);
            // Double limit = raceInfo.maxSpeedLimit != null ? raceInfo.maxSpeedLimit : 0.1;
            // gameInit.getTrack().physics.setMaxSpeedAtRadius(limit, 90);
          }
          CarDimensionWrapper dim = gameInit.getCarDimensions(raceName);
          logger.log(LogLevel.ACTIONRULE_USED, "Ruukki mitat: " + dim.length + "x" + dim.width, raceInfo);
          logger.logGameInit(line, raceInfo);
          logger.log(LogLevel.MESSAGE, line, raceInfo);
        }
        else if (msgFromServer.msgType.equals("gameEnd")) {
          gameEnd.fromJson(line);
          logger.logGameEnd(line);
          logger.log(LogLevel.MESSAGE, line, raceInfo);
        }
        else if (msgFromServer.msgType.equals("gameStart")) {
          logger.log(LogLevel.PROGRESS, line, raceInfo);
          logger.log(LogLevel.MESSAGE, line, raceInfo);

          sendThrottle(1.0, 0);
        }
        else if (msgFromServer.msgType.equals("lapFinished")) {
          lapFinished = gson.fromJson(line, LapFinished.class);
          if (lapFinished.getData().getCar().getName().equals(raceInfo.raceName))
          {
            gameInit.getTrack().setNewLap();

            logger.logLapFinished(line);
            logger.log(LogLevel.MESSAGE, line, raceInfo);
            
            if (gui != null)
            {
              if (gui.getLaps() == lapFinished.getData().getLapTime().getLap() + 1)
              {
                // Break
                break;
              }
            }
          }
        }
        else if (msgFromServer.msgType.equals("crash")) {

          Crash crash = crashes.addCrash(line);
          if (crash.getData().getName().equals(raceInfo.raceName))
          {

            doCrash();

            logger.log(LogLevel.CRASH, "CARSHED", raceInfo);
            logger.log(LogLevel.CURVEANGLE, ", CRASH", raceInfo);
            logger.logCrash(crash);
            logger.log(LogLevel.MESSAGE, line, raceInfo);
          }
        }
        else if (msgFromServer.msgType.equals("spawn")) {
          spawn = gson.fromJson(line, Spawn.class);
          if (spawn.getData().getName().equals(raceInfo.raceName))
          {
            logger.logSpawn(line);
            logger.log(LogLevel.MESSAGE, line, raceInfo);
            driver.youContinue();
          }
        }
        else if (msgFromServer.msgType.equals("tournamentEnd")) {
          logger.log(LogLevel.PROGRESS, "Tournament End", raceInfo);
          logger.log(LogLevel.MESSAGE, line, raceInfo);
        }
        else if (msgFromServer.msgType.equals("finish")) {
          finish = gson.fromJson(line, Finish.class);
          if (finish.getData().getName().equals(raceInfo.raceName))
          {
            logger.logFinish(line);
            logger.log(LogLevel.MESSAGE, line, raceInfo);
          }
        }
        else if (msgFromServer.msgType.equals("dnf")) {
          dnf = gson.fromJson(line, Dnf.class);
          if (dnf.getData().getCar().getName().equals(raceInfo.raceName))
          {
            logger.logDnf(line);
            logger.log(LogLevel.MESSAGE, line, raceInfo);
          }
        }
        else if (msgFromServer.msgType.equals("yourCar")) {
          yourCar = gson.fromJson(line, YourCar.class);
          String name = yourCar.getData().getName();
          String color = yourCar.getData().getColor();
          logger.log(LogLevel.ACTIONRULE_USED, "autosi: " + name + "(" + color + ")", raceInfo);
          logger.log(LogLevel.MESSAGE, line, raceInfo);
          logger.log(LogLevel.PROGRESS, line, raceInfo);

        }
        else if (msgFromServer.msgType.equals("turboAvailable")) {
          turboAvailable = gson.fromJson(line, TurboAvailable.class);

          doTurboAvailableTick();

          logger.logTurboAvailable(line);
          logger.log(LogLevel.MESSAGE, line, raceInfo);

        }
        else if (msgFromServer.msgType.equals("turboStart")) {
          turbostart = gson.fromJson(line, TurboStart.class);
          if (turbostart.getData().getName().equals(raceInfo.raceName))
          {
            doTurboStart();
            logger.log(LogLevel.ACTIONRULE_USED, "turbo started", raceInfo);
            logger.log(LogLevel.MESSAGE, line, raceInfo);
            logger.log(LogLevel.PROGRESS, line, raceInfo);
          }
        }
        else if (msgFromServer.msgType.equals("turboEnd")) {
          turboend = gson.fromJson(line, TurboEnd.class);
          if (turboend.getData().getName().equals(raceInfo.raceName))
          {
            doTurboEnd();
            logger.log(LogLevel.ACTIONRULE_USED, "turbo stopped/not available", raceInfo);
            logger.log(LogLevel.MESSAGE, line, raceInfo);
            logger.log(LogLevel.PROGRESS, line, raceInfo);
          }
        }
        else {
          logger.logUnknown(line);
          logger.log(LogLevel.MESSAGE, line, raceInfo);
          send(new Ping());
        }
      } catch (Exception e) {

        e.printStackTrace();
        System.out.println("Some Exception. Send Ping" + e);
        try {
          // Send something
          sendThrottle(0.5, carPositions.getGameTick());
        } catch (Exception e1) {
          // TODO Auto-generated catch block
          e1.printStackTrace();
        }
      }

      // Be sure to send something
      // send(new Ping());
    }

    logger.log(LogLevel.PROGRESS, "No more messages received!", raceInfo);
    logger.log(LogLevel.MESSAGE, "No more messages received!", raceInfo);
    logger.closeLogger();
    closeSocket();
  }

  private void doCrash() throws Exception {
    // gameInit.getTrack().physics.adjustMaxCentrifugalF(0.9);

    // myRaceCar.getPiece().setCrashesInthisCurveBackwards(200.0);
    // myRaceCar.getPiece().setCrashesInthisCurveForwards(200.0);

    PieceWrapper piece = myRaceCar.getPiece();
    CurveType type = piece.getCurveType();

    while (type == null)
    {
      type = piece.getPrevPiece().getCurveType();
    }
    gameInit.getTrack().setDangerousCurveType(type);
    logger.log(LogLevel.PROGRESS, "type:" + type.crashes + "D" + type.dir + "R" + type.radius + "L" + type.length, raceInfo);

    logger.log(LogLevel.ACTIONRULE_USED, "adjusted the curve speed", raceInfo);
    driver.youCrashed();
  }

  private void doTurboEnd() throws Exception {
    // Tell the race car that the turbo has deactivated
    myRaceCar.setTurboOff();

  }

  private void doTurboStart() throws Exception {
    // Tell the race car that the turbo has activated
    myRaceCar.setTurboOn();

  }

  private void doTurboAvailableTick() throws Exception {
    int curTick = carPositions.getGameTick();

    myRaceCar.provideTurbo(turboAvailable, curTick);
    considerTurbo();
  }

  private void considerTurbo() throws Exception {
    boolean doTurboNow = myRaceCar.checkIfTurboIsGoodIdea();

    if (doTurboNow)
    {
      // request turbo
      myRaceCar.turboSent = true;
      Turbo msg = JsonMsgBuilder.getTurboMsg();
      String json = gson.toJson(msg);
      sendJson(json);
      logger.log(LogLevel.MESSAGE, json, raceInfo);
    }
  }

  private void doCarPosTick() throws Exception {

    considerTurbo(); // Send turbo msg to server if a good choice

    Double nextValue = driver.getNextAction(carPositions);
    if (nextValue == TrackWrapper.LEFT)
    {
      send(new SwitchLane("Left"));
      carPositions.getMyCar().getPiece().setSwitchSent(TrackWrapper.LEFT);
    }
    else if (nextValue == TrackWrapper.RIGHT)
    {
      SwitchLane msg = new SwitchLane("Right");
      send(msg);
      carPositions.getMyCar().getPiece().setSwitchSent(TrackWrapper.RIGHT);
      // logger.log(LogLevel.DETAIL, "S" + carPositions.getGameTick() + " ", raceInfo);
      logger.log(LogLevel.PROGRESS, "json:" + msg.toJson(), raceInfo);
    }
    else if (nextValue >= TrackWrapper.NONE)
    {
      sendThrottle(nextValue, carPositions.getGameTick());
    }
    else
    {
      // Send something
      nextValue = 0.5;
      sendThrottle(nextValue, carPositions.getGameTick());

      System.out.println("unexpected value of nextValue:" + nextValue);
    }

    logger.logCarpositions(carPositions);
  }

  private void sendThrottle(Double nextValue, Integer gameTick) throws Exception {
    Throttle msg = new Throttle();
    msg.setGameTick(gameTick);
    msg.setMsgType("throttle");
    msg.setData(nextValue);
    String json = gson.toJson(msg);
    sendJson(json);

    logger.log(LogLevel.CARPOS_MESSAGE, json, raceInfo);
    logger.logToGui(DashCauge.THROTTLE, String.format("%.2f", nextValue));
  }

  private void sendJson(String msg) throws Exception {
    writer.println(msg);
    writer.flush();

    logger.log(LogLevel.MESSAGE, msg, raceInfo);
  }

  private void send(final SendMsg msg) throws Exception {
    writer.println(msg.toJson());
    writer.flush();
  }

  private void closeSocket() {
    try {
      socket.close();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  public RaceInfo getRaceInfo() throws Exception {
    return raceInfo;
  }

  public RaceCars getRaceCars() throws Exception {
    return raceCars;
  }

}

abstract class SendMsg {
  public String toJson() {
    return new Gson().toJson(new MsgWrapper(this));
  }

  protected Object msgData() {
    return this;
  }

  protected abstract String msgType();
}

class MsgWrapper {
  public final String msgType;
  public final Object data;

  MsgWrapper(final String msgType, final Object data) {
    this.msgType = msgType;
    this.data = data;
  }

  public MsgWrapper(final SendMsg sendMsg) {
    this(sendMsg.msgType(), sendMsg.msgData());
  }
}

class Join extends SendMsg {
  public final String name;
  public final String key;

  Join(final String name, final String key) {
    this.name = name;
    this.key = key;
  }

  @Override
  protected String msgType() {
    return "join";
  }
}

class Ping extends SendMsg {
  @Override
  protected String msgType() {
    return "ping";
  }
}

class SwitchLane extends SendMsg {
  private String value;
  private Integer tick;

  public SwitchLane(String value) {
    this.value = value;
    this.tick = tick;
  }

  @Override
  protected Object msgData() {
    return value;
  }

  @Override
  protected String msgType() {
    return "switchLane";
  }
}
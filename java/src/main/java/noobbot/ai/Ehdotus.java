package noobbot.ai;

public class Ehdotus
{
  public String key;
  public Double value = 1.0;
  public Weight weight = Weight.DEFAULT;

  public Ehdotus()
  {
  }

  public void asetaPaluuArvo(String newkey, Double newvalue, Weight newweight) throws Exception {
    if (newweight.value < this.weight.value)
    {
      // ei muutosta koska t�rke�mpi asetus on voimassa
      return;
    }
    if (newweight.value > this.weight.value || newvalue < this.value)
    {
      // uusi asetus on t�rke�mpi tai sitten se on pienempi jolloin se pit�� ottaa huomioon
      this.key = newkey;
      this.value = newvalue;
      this.weight = newweight;
    }
  }

  public enum Weight
  {
    START(10),
    SPEED(1), // 5
    SLIP(0),
    STRAIGHT(2),
    ANGLE(5), // 1
    MIN(2), // Atlest this much, not ilmplemented
    DEFAULT(0);

    private int value;

    private Weight(int value) {
      this.value = value;
    }

  }

}

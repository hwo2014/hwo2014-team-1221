package noobbot.ai;

import java.util.ArrayList;
import java.util.List;

import noobbot.hwo.turboavailable.TurboAvailable;
import noobbot.wrappers.CarWrapper;

public class MyRaceCar extends RaceCar {

  private List<CarWrapper> raceHistory = new ArrayList<CarWrapper>();
  private int turboAvailableUntil;
  private Double turboFactor = 0.0;
  private TurboAvailable turboAvailable;
  public boolean turboSent = false;

  public List<CarWrapper> getRaceHistory() throws Exception {
    return raceHistory;
  }

  public MyRaceCar()
  {
    super();
  }

  public void addRaceHistory(String key, Double value) throws Exception
  {
    carInfo.setRecordedAction(key);
    carInfo.setRecordedThrottle(value);
    carInfo.setRecordedSpeed(getSpeed());
    raceHistory.add(carInfo);
  }

  public void provideTurbo(TurboAvailable turboAvailable, Integer curTick) throws Exception {

    this.turboSent = false;
    this.turboAvailable = turboAvailable;
    int turboTicks = turboAvailable.getData().getTurboDurationTicks();
    turboAvailableUntil = curTick + turboTicks;
  }

  public boolean checkIfTurboIsGoodIdea() throws Exception {

    if (!turboSent && (carPositions.getGameTick() != null && turboAvailableUntil < carPositions.getGameTick()))
    {
      if (turboAvailableUntil == carPositions.getGameTick() + 1)
      {
        // If turboavailable is about to expire then start turbo
        return true;
      }

      Double distanceToCurve = getPiece().getDistanceToCurve();
      if ((distanceToCurve > 200 || needToAccelerate()) && !needToDecelerate())
        return true;
    }
    return false;
  }

  private boolean needToDecelerate() throws Exception {

    int currentLaneIndex = getLane().getEndLaneIndex();
    return getSpeed() - 0.1 > getPiece().getMaxSpeedOfNextCurvePiece(currentLaneIndex, null);
  }

  private boolean needToAccelerate() throws Exception {
    int currentLaneIndex = getLane().getEndLaneIndex();
    return getSpeed() + 0.1 < getPiece().getMaxSpeedOfNextCurvePiece(currentLaneIndex, null);
  }

  public boolean isTurboAvailable(Integer curTick) throws Exception
  {
    return curTick <= turboAvailableUntil;
  }

  public void setTurboOn() throws Exception
  {
    turboFactor = turboAvailable.getData().getTurboFactor();
  }

  public void setTurboOff() throws Exception
  {
    turboFactor = 0.0;
  }

  public boolean isTurboOn() throws Exception {
    return turboFactor > 0;
  }

  public Double getTurboFactor() throws Exception {
    return turboFactor;
  }

  public boolean isNextOnInnerlane() throws Exception {
    return carInfo.isNextOnInnerlane();
  }

  public int moveTowardsInnerlane() throws Exception {
    return carInfo.moveTowardsInnerlane();
  }

  public Double getTargetSpeed() throws Exception {
    return getPiece().getMaxSpeedOfNextCurvePiece(carInfo.getLane().getEndLaneIndex(), null);
  }
}

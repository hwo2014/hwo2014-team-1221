package noobbot.ai;

import noobbot.Main;
import noobbot.ai.Ehdotus.Weight;
import noobbot.logging.LogLevel;
import noobbot.logging.Logger;
import noobbot.wrappers.CarPositionsWrapper;
import noobbot.wrappers.GameInitWrapper;
import noobbot.wrappers.PieceWrapper;
import noobbot.wrappers.TrackWrapper;

public class Driver {

  private static final int keepclosetozeroConst = 1;

  private static final double BUMPING_DISTANCE = 150.0;

  // Distance to use when calculating the max speed of the track
  public static final double DISTANCE_MAXSPEED_CALC = 300.0;

  // Distance and speed threshold values on a straight

  // Top distance to curve and speed threshold between the top distance
  // and STRAIGHT_DISTANCE_TO_CURVE2 (200-400)
  private static final int STRAIGHT_TOP_DISTANCE_TO_CURVE1 = 400; // 300 crashed in us
  private static final double STRAIGHT_MAXSPEED_THRESHOLD_CLOSING_CURVE2 = 5.0; // 6.0 crashed in US

  // Second distance to curve and speed threshold below that distance
  private static final int STRAIGHT_DISTANCE_TO_CURVE2 = 200;
  private static final double STRAIGHT_MAXSPEED_THRESHOLD_CLOSING_CURVE1 = 3.0; // 3.5 fails in germany // 3.2 failed in keimola

  // Distance to curve when throttle OFF when exceeds maxSpeed
  private static final int STRAIGHT_DISTANCE_CURVE3 = 100;

  // Distance, speed and angle threshold values on a curve

  // Speed threshold less than max speed for setting full throttle
  private static final double SPEED_THRESHOLD_MARGIN_FOR_FULL_THROTTLE = 0.2;
  // Speed threshold over max speed
  private static final double EXCESS_SPEED_THRESHOLD = 0.01;

  // Test purposes
  private static final Integer GAMETICKTHRESSHOLD_FULLTHROTTLE = 1; // 81
  private static final Double GAMETICK_FULLTHROTTLE = 0.5; // 1.0
  private static final double TICK_DECELERATION_SPEEDTHRESHOLD = 40.0;

  private GameInitWrapper gameInit;
  private CarPositionsWrapper carPositions;

  // CarWrapper myCar;
  MyRaceCar myRaceCar;
  private Ehdotus ehdotus = new Ehdotus();
  private Double prevThrottleValue = 0.0;

  private Logger logger = Logger.getInstance();

  private Double startCurveSpeed;

  private boolean debugFlagDeaccelerated;

  private boolean debugFlagDeaccelerated2;

  private LaneAdvicer laneAdvicer;

  private Main main;

  private int firstTicksAtFirstCurve = 10000;

  private boolean dragConstantCalculated;

  private boolean calculateDragConstant;

  private int keepCloseToZero = keepclosetozeroConst;

  private boolean slipStarted = false;

  private int ensikerrallaKiihdyta = 0;

  private Double startSpeed;

  public Driver(Main main) {
    this.gameInit = main.gameInit;
    this.myRaceCar = main.myRaceCar;
    this.main = main;
  }

  /**
   * Determines what to do next. A Double value greater or equal to 0 means to send
   * the Throttle message with the given value. -1 means to switch lanes to the left.
   * -2 means to switch lanes to the right
   * 
   * @param carPositions
   * 
   * @return
   */
  public Double getNextAction(CarPositionsWrapper carPositions) throws Exception
  {

    this.carPositions = carPositions;
    ehdotus = new Ehdotus(); // alustetaan uusi ehdotus
    // myCar = carPositions.getMyCar();

    determineActionByGameTick(carPositions.getGameTick());

    Integer pieceIndex = myRaceCar.getPieceIndex();
    int laneIndexToGetSpeed = myRaceCar.getLane().getEndLaneIndex();

    // Katsotaan ensiksi pitaisiko vaihtaa kaistaa
    this.laneAdvicer = new LaneAdvicer(this, gameInit.getTrack(), main.getRaceInfo());
    int laneChange = laneAdvicer.considerLaneChangeAdvanced();

    if (laneChange != TrackWrapper.NONE)
    {
      // Vaihdetaan kaistaa
      return (double)laneChange;
    }

    // Double throttle = considerBumpSpeed();
    // if (throttle != null)
    // return throttle;

    // Consider straight/curved
    boolean isMyCarOnStraight = myRaceCar.getPiece().isStreight();
    if (isMyCarOnStraight)
    {
      // if (firstTicksAtFirstCurve != 0 && (ensikerrallaKiihdyta > 0 || myRaceCar.getSpeed() > 4.5))
      // {
      // // Keep constant speed until next curve
      // return keepConstantSpeed();
      // }

      startCurveSpeed = (Double)null; // used for testing
      Double maxSpeed = myRaceCar.getPiece().getMaxSpeedOfNextCurvePiece(laneIndexToGetSpeed, null);
      return considerStraight(maxSpeed);
    }
    else
    {
      calculateDragConstant = false; // Set to false just in case the calculation was initiated but not finnished on a next straight

      if (false)
        return keepConstantSpeedForTest();

      if (firstTicksAtFirstCurve != 0)
      {
        firstTicksAtFirstCurve--;

        Double maxSpeedInThisCurve = DetermineMaxSpeedInThisCurve();
        if (maxSpeedInThisCurve != 0.0)
        {
          int r = myRaceCar.getPiece().getLaneRadius(myRaceCar.getLane().getEndLaneIndex());
          gameInit.getTrack().physics.setFriction(maxSpeedInThisCurve, r);
          firstTicksAtFirstCurve = 0;
        }
      }
      Double inPieceDistance = myRaceCar.getInPieceDistance();
      // Determine max speed on the track
      Double trackMaxSpeed = gameInit.getTrack().getTrackMaxSpeed(pieceIndex, laneIndexToGetSpeed, inPieceDistance, DISTANCE_MAXSPEED_CALC);

      return considerCurved(trackMaxSpeed);
    }
  }

  private Double DetermineMaxSpeedInThisCurve() throws Exception {

    Double prevSpeed = myRaceCar.getPrevCarSpeed();

    if (myRaceCar.getAngle() == 0.0)
    {
      if (ensikerrallaKiihdyta > 0)
      {
        ensikerrallaKiihdyta++;
        asetaPaluuArvo("kiihdyt� jotta slip alkaisi", 1.0, Weight.SLIP);

        // Then slip angle has not been reached. The minimum friction can be set.
        // Add some extra in order to force acceleration
        gameInit.getTrack().physics.setFrictionIfGreater(myRaceCar.getSpeed() * 0.1,
            myRaceCar.getPiece().getLaneRadius(myRaceCar.getLane().getEndLaneIndex()));

      }
      else
        ensikerrallaKiihdyta = 1; // toisella kerralla kiihdytet��n jos angle viel�kin nolla
    }
    else if (myRaceCar.getPrevCarAngleDiff() > 0 || Math.abs(myRaceCar.getAngle()) > 5)
    {
      asetaPaluuArvo("slow down to get 0 angle", 0.0, Weight.ANGLE);
      slipStarted = true;
    }
    else
    {
      if (ensikerrallaKiihdyta > 1)
        slipStarted = true; // nyt on slip alkamaan kiihdytyksen j�lkeen

      if (myRaceCar.isTurboOn())
      {
        // Keep the speed
        Double turboFactor = myRaceCar.getTurboFactor();
        asetaPaluuArvo("keep the maxspeed in the curve to get 0 angle (turbo)", prevSpeed / (10 * turboFactor), Weight.ANGLE);
      }
      else
      {
        asetaPaluuArvo("keep the maxspeed in the curve to get 0 angle", prevSpeed / 10, Weight.ANGLE);
      }
    }

    if (Math.abs(myRaceCar.getAngle()) < 1.0 && slipStarted)
    {
      keepCloseToZero--;

      if (keepCloseToZero == 0 || ensikerrallaKiihdyta > 1)
      {
        if (Math.abs(myRaceCar.getAngle()) < 0.2)
          return myRaceCar.getSpeed();
        else if (Math.abs(myRaceCar.getAngle()) < 0.4)
          return myRaceCar.getSpeed() * 0.93;
        else if (Math.abs(myRaceCar.getAngle()) < 0.5)
          return myRaceCar.getSpeed() * 0.83;
        else
          return myRaceCar.getSpeed() * 0.73;
      }
      else
      {
        return 0.0;
      }
    }
    else
    {
      keepCloseToZero = keepclosetozeroConst;
      return 0.0;
    }

  }

  private void adjustAccordingToMaverikou() throws Exception {
    main.gameInit.getTrack().physics.adjustMaxSpeed(
        myRaceCar.getSpeed(),
        myRaceCar.getAngle(),
        myRaceCar.getPiece().getLaneRadius(myRaceCar.getLane().getEndLaneIndex()));

    Double newMaxSpeed = main.gameInit.getTrack().physics.getMaximumSpeed(myRaceCar.getPiece().getRadius());
    logger.log(LogLevel.ACTIONRULE_USED, "newMaxSpeed=" + newMaxSpeed + "(" + main.gameInit.getTrack().physics.maxCentrifugalF + ")",
        main.getRaceInfo());
  }

  private Double considerBumpSpeed() throws Exception {

    RaceCar closeCar = main.getRaceCars().getClosestCarOnLane(BUMPING_DISTANCE);
    if (closeCar == null)
      return null;

    return asetaJaPalautaPaluuArvo("BUMPING_DISTANCE", 1.0, Weight.SPEED);
  }

  private Double considerStraight(Double maxSpeed) throws Exception {

    // considerCarAngle();

    // Calculate the distance to the next curve
    Double distanceToCurve = myRaceCar.getPiece().getDistanceToCurve() - myRaceCar.getInPieceDistance();
    Double speed = myRaceCar.getSpeed();

    if (calculateDragConstant)
    {
      // Calculate drag constant at (first) zero throttle
      main.gameInit.getTrack().physics.setDragConstant(myRaceCar.getPrevCarSpeed(), speed);
      logger.log(LogLevel.AI, "Calc Dragconstant: Prevcarspeed: " + myRaceCar.getPrevCarSpeed() + ";S" + speed, main.getRaceInfo());

      calculateDragConstant = false;
      dragConstantCalculated = true;
    }

    if (dragConstantCalculated)
    {
      considerStraightWithDragConstant(distanceToCurve, speed, maxSpeed);
    }
    else
    {
      considerStraightBeforeDragConstant(distanceToCurve, speed, maxSpeed);
    }
    Double arvo = palautaEdotettuArvo();

    if (!dragConstantCalculated && speed > 0.3)
    {
      // do only once
      // drag constant shall be calculated at next tick where the throttle value has been 0.0
      calculateDragConstant = true;
      arvo = 0.0;
    }

    return arvo;

  }

  private void considerCarAngle() throws Exception {

    // Tarkista ensin ettei pera heita liian nopeasti
    if (Math.abs(myRaceCar.getPrevCarAngleDiff()) >= 4)
      asetaPaluuArvo("angle diff >= 4", 0.0, Weight.ANGLE);

    if (myRaceCar.getAngle() > 50)
      asetaPaluuArvo("angle greater > 50", 0.0, Weight.ANGLE);
  }

  private void considerStraightWithDragConstant(Double distanceToCurve, Double speed, Double maxSpeed) {

    Double distanceToMaxSpeed = null;
    try {
      Physics physics = main.gameInit.getTrack().physics;
      distanceToMaxSpeed = physics.distanceToSpeed(maxSpeed, speed);

      String distCurve = String.format("%.2f", distanceToCurve);
      String distMax = String.format("%.2f", distanceToMaxSpeed);
      String speedsStr = String.format("%.2f", maxSpeed) + ":" + String.format("%.2f", speed);

      if (distanceToCurve - speed > distanceToMaxSpeed) // minus the current speed so we don't go over
      {
        // Full throttle
        asetaPaluuArvo("distanceToCurve(" + distCurve + ") > " + "distanceToMaxSpeed(" + distMax + ")" + speedsStr, 1.0, Weight.SPEED);
      }
      else
      {
        // Zero throttle
        asetaPaluuArvo("distanceToMaxSpeed reached(" + distMax + ")distCurve=(" + distCurve + ")", 0.0, Weight.SPEED);
      }
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      System.out.println("considerStraightWithDragConstant: " + distanceToMaxSpeed + "D" + distanceToCurve + "S" + speed + "M" + maxSpeed);
    }

  }

  private void considerStraightBeforeDragConstant(Double distanceToCurve, Double speed, Double maxSpeed) throws Exception {
    if (distanceToCurve > STRAIGHT_DISTANCE_TO_CURVE2)
    {
      Double maxSpeedThreshold = STRAIGHT_MAXSPEED_THRESHOLD_CLOSING_CURVE2;

      if (distanceToCurve < STRAIGHT_TOP_DISTANCE_TO_CURVE1 && speed > maxSpeed + maxSpeedThreshold)
        asetaPaluuArvo("speed > maxSpeed + " + maxSpeedThreshold, 0.0, Weight.SPEED);

      asetaPaluuArvo("distance to Curve > " + STRAIGHT_DISTANCE_TO_CURVE2, 1.0, Weight.SPEED);
    }
    else
    {
      // Lahestytaan kurvia

      // Oletusarvoisesti mennaan viela taysilla
      asetaPaluuArvo("distance to Curve <= " + STRAIGHT_DISTANCE_TO_CURVE2, 1.0, Weight.SPEED);

      // Tarkistetaan kuitenkin viela nopeus
      Double maxSpeedThreshold = STRAIGHT_MAXSPEED_THRESHOLD_CLOSING_CURVE1;
      if (speed > maxSpeed + maxSpeedThreshold)
        asetaPaluuArvo("speed > maxSpeed + " + maxSpeedThreshold, 0.0, Weight.SPEED);

      if (distanceToCurve < STRAIGHT_DISTANCE_CURVE3)
      {
        if (speed > maxSpeed)
        {
          asetaPaluuArvo("distanceToCurve < " + STRAIGHT_DISTANCE_CURVE3 + ";speed > maxSpeed(" + String.format("%.2f", maxSpeed) + ")",
              0.0, Weight.SPEED);
        }
      }
    }
  }

  /**
   * Given TRACK max speed
   * 
   * @param maxSpeed
   * @return
   */
  private Double considerCurved(Double maxSpeed) throws Exception {

    // Tarkista ensin ettei pera heita liian nopeasti
    // if (Math.abs(myRaceCar.getPrevCarAngleDiff()) >= 2)
    // return asetaJaPalautaPaluuArvo("angle diff >= 2", 0.0, Weight.SPEED);
    //
    // if (myRaceCar.getAngle() > 50)
    // return asetaJaPalautaPaluuArvo("angle greater > 50", 0.0, Weight.SPEED);

    if (maxSpeed != null)
    {
      if (myRaceCar.getSpeed() > maxSpeed)// + EXCESS_SPEED_THRESHOLD) // add a threshold value
        asetaPaluuArvo("speed > maxspeed", 0.0, Weight.SPEED);
      else if (myRaceCar.getSpeed() < maxSpeed - SPEED_THRESHOLD_MARGIN_FOR_FULL_THROTTLE) // 2.0 crashed in finland
        // in case the speed has fallen well beyond the maximum then step on.
        asetaPaluuArvo("speed closer to the max speed", 1.0, Weight.SPEED);
      else
      {
        if (myRaceCar.isTurboOn())
        {
          Double turboFactor = myRaceCar.getTurboFactor();
          asetaPaluuArvo("keep the maxspeed in the curve (turbo)", maxSpeed / (10 * turboFactor), Weight.SPEED);
        }
        else
        {
          asetaPaluuArvo("keep the maxspeed in the curve", maxSpeed / 10, Weight.SPEED);
        }
      }
    }
    else
      asetaPaluuArvo("olinull", 0.5, Weight.SPEED); // Should not happen!

    // ADDE HERE TO COMPENSATE FOR THE CHANGING PHYSICS (DIRTY FIX)
    // double angle = Math.abs(myCar.getAngle());
    // // Go slower if the angle too great
    // if (angle > KAARREKULMA1)
    // asetaPaluuArvo("KAARREKULMA1_KAASU", KAARREKULMA1_KAASU, Weight.ANGLE);
    // else if (angle > KAARREKULMA2)
    // asetaPaluuArvo("KAARREKULMA2_KAASU", KAARREKULMA2_KAASU, Weight.ANGLE);
    // else if (angle > KAARREKULMA3)
    // asetaPaluuArvo("KAARREKULMA3_KAASU", KAARREKULMA3_KAASU, Weight.ANGLE);
    // else if (angle > KAARREKULMA4)
    // asetaPaluuArvo("KAARREKULMA4_KAASU", KAARREKULMA4_KAASU, Weight.ANGLE);
    // else
    // asetaPaluuArvo("KAARRE_KAASU", KAARRE_KAASU, Weight.ANGLE);

    return palautaEdotettuArvo();
  }

  private void asetaPaluuArvo(String key, Double value, Weight weight) throws Exception {
    ehdotus.asetaPaluuArvo(key, value, weight);
  }

  private Double asetaJaPalautaPaluuArvo(String key, Double value, Weight weight) throws Exception {
    asetaPaluuArvo(key, value, weight);
    return palautaEdotettuArvo();

  }

  private Double palautaEdotettuArvo() throws Exception
  {

    String ehdotettuKey = ehdotus.key;
    Double ehdotettuArvo = ehdotus.value;

    myRaceCar.addRaceHistory(ehdotettuKey, ehdotettuArvo);

    Integer radius = myRaceCar.getPiece().getRadius();

    String pre;
    if (radius != null)
      pre = " ( (";
    else
      pre = " | |";
    String tick = pre + " T" + carPositions.getGameTick();
    Integer pieceIndex = myRaceCar.getPieceIndex();
    String piece = " P" + pieceIndex;
    PieceWrapper currentPiece = gameInit.getTrack().getPiece(pieceIndex);
    if (currentPiece.isSwitchable())
    {
      piece = " S" + pieceIndex;
    }

    int laneIndex = myRaceCar.getLane().getEndLaneIndex();
    Double maxSpeed = currentPiece.getMaxSpeedOfNextCurvePiece(laneIndex, null);
    Double trackMaxSpeed = gameInit.getTrack().getTrackMaxSpeed(pieceIndex, laneIndex, myRaceCar.getInPieceDistance(),
        DISTANCE_MAXSPEED_CALC);
    if (radius != null)
    {
      piece = piece + " R" + radius + " M" + String.format("%.2f", maxSpeed);
      piece = piece + "(" + String.format("%.2f", trackMaxSpeed) + ")";
    }
    else
    {
      piece = piece + " M" + String.format("%.2f", maxSpeed);
    }
    String speedStr = " S" + myRaceCar.getSpeedStr(2);
    String angleStr = " A" + myRaceCar.getAngle();
    angleStr = angleStr + "(" + myRaceCar.getPrevCarAngleDiff() + ")";
    String speedDelta = "";
    String dragConstant = "";
    String frictionStr = "";
    if (myRaceCar.getPrevCarSpeed() != null && myRaceCar.getSpeed() != null && !offTrack)
    {
      speedDelta = "(" + String.format("%.2f", myRaceCar.getSpeed() - myRaceCar.getPrevCarSpeed()) + ")";

      // Double k = Math.log(myRaceCar.getSpeed() / myRaceCar.getPrevCarSpeed());
      Double k = gameInit.getTrack().physics.getDragConst();
      dragConstant = "(" + k + ")";

      Double maxCF = gameInit.getTrack().physics.getCENTRIPETAL_ACCELERATION();
      Double friction = null;
      if (radius != null)
      {
        Integer laneRadius = myRaceCar.getPiece().getLaneRadius(myRaceCar.getLane().getEndLaneIndex());
        // friction = Math.pow(myRaceCar.getSpeed(), 2) / (laneRadius * 9.8);
        frictionStr = " F" + main.gameInit.getTrack().physics.getFriction();

        // TICK, SPEED, MAXSPEED, Centrifugal, curCF, (RADIUS), ANGLE, ANGLEDELTA, (FRICTION)

        Double speed = myRaceCar.getSpeed();
        Double CalcCF = speed * speed / laneRadius;

        String curveInfo =
            tick
                + ", " + speed
                + ", " + trackMaxSpeed
                + ", " + maxCF
                + ", " + CalcCF
                + ", " + laneRadius
                + ", " + myRaceCar.getAngle()
                + ", " + myRaceCar.getPrevCarAngleDiff()
                + ", " + friction
                + ", " + ehdotettuKey + ":" + ehdotettuArvo;

        logger.log(LogLevel.CURVEANGLE, curveInfo, main.getRaceInfo());

      }
      else
      {
        String curveInfo =
            tick
                + ", " + myRaceCar.getSpeed()
                + ", " + maxSpeed
                + ", " + maxCF
                + ", 0"
                + ", 0"
                + ", " + myRaceCar.getAngle()
                + ", " + myRaceCar.getPrevCarAngleDiff()
                + ", 0"
                + ", " + ehdotettuKey + ":" + ehdotettuArvo;
        logger.log(LogLevel.CURVEANGLE, curveInfo, main.getRaceInfo());
      }
    }

    if (!offTrack)
    {
      String msg = tick + piece + speedStr + speedDelta + dragConstant + frictionStr + angleStr + ":" + ehdotettuKey + ":" + ehdotettuArvo;
      logger.log(LogLevel.ACTIONRULE_USED, msg, main.getRaceInfo());
    }

    if (ehdotettuArvo >= 0)
      prevThrottleValue = ehdotettuArvo;

    return ehdotettuArvo;

  }

  private Double keepConstantSpeedForTest() throws Exception {
    Double thr = 0.5; // default if null
    if (startCurveSpeed == null)
    {
      startCurveSpeed = myRaceCar.getSpeed();
    }
    if (startCurveSpeed != null)
      thr = startCurveSpeed / 10;
    asetaPaluuArvo("speed", thr, Weight.ANGLE);

    return palautaEdotettuArvo();
  }

  private Double keepConstantSpeed() throws Exception {
    Double thr = 0.5; // default if null
    if (startSpeed == null)
    {
      startSpeed = myRaceCar.getSpeed();
    }
    if (startSpeed != null)
      thr = startSpeed / 10;
    asetaPaluuArvo("keep constant speed until next curve to get slip angle", thr, Weight.SPEED);

    return palautaEdotettuArvo();
  }

  private void determineActionByGameTick(Integer gameTick) throws Exception {

    if (gameTick != null && gameTick < GAMETICKTHRESSHOLD_FULLTHROTTLE)
    {
      if (false && (myRaceCar.getSpeed() >= TICK_DECELERATION_SPEEDTHRESHOLD || debugFlagDeaccelerated))
      {
        asetaPaluuArvo("debugFlagDeaccelerated", 0.0, Weight.START);
        debugFlagDeaccelerated = true;
      }
      else
      {
        logger.logGameTick();
        asetaPaluuArvo("GAMETICKTHRESSHOLD_FULLTHROTTLE", GAMETICK_FULLTHROTTLE, Weight.START);
      }
    }
  }

  private Double testDecelerationStraight() throws Exception {

    if (myRaceCar.getSpeed() >= 9.0 || debugFlagDeaccelerated2)
    {
      debugFlagDeaccelerated2 = true;
      asetaPaluuArvo("debugFlagDeaccelerated2", 0.0, Weight.START);
    }

    return palautaEdotettuArvo();
  }

  // SUORA kaasulukemat:
  final Double SUORA0_LIIKANOPEUS2 = 9.0;
  final Double SUORA0_LIIKANOPEUS2_KAASU = 0.0;

  final Double SUORA0_LIIKANOPEUS1 = 7.0;
  final Double SUORA0_LIIKANOPEUS1_KAASU = 0.1;

  final Double SUORA1_LIIKANOPEUS = 9.0;
  final Double SUORA1_LIIKANOPEUS_KAASU = 0.0;

  final Double SUORA0_KAASU = 0.51;
  final Double SUORA1_KAASU = 1.0;
  final Double SUORA2_KAASU = 1.0;
  final Double SUORA3_KAASU = 1.0;
  final Double SUORA_KAASU = 1.0;

  // Kaarre kaasulukemat:
  final Double KAARRE_LIIKANOPEUS2 = 10.0; // 6.5
  final Double KAARRE_LIIKANOPEUS2_KAASU = 0.1;
  final Double KAARRE_LIIKANOPEUS1 = 10.0; // 6.0
  final Double KAARRE_LIIKANOPEUS1_KAASU = 0.3;
  final Double KAARREKULMA1 = 50.0;
  final Double KAARREKULMA1_KAASU = 0.0;
  final Double KAARREKULMA2 = 40.0;
  final Double KAARREKULMA2_KAASU = 0.45;
  final Double KAARREKULMA3 = 30.0;
  final Double KAARREKULMA3_KAASU = 0.55;
  final Double KAARREKULMA4 = 20.0;
  final Double KAARREKULMA4_KAASU = 0.65;
  final Double KAARRE_KAASU = 0.75;

  private boolean offTrack = false;

  private Double considerCurved(Double maxSpeed, Double currentAcceleration) throws Exception {

    if (currentAcceleration == null)
    {
      asetaPaluuArvo("currentAcceleration == null", 1.0, Weight.ANGLE);
    }

    if (myRaceCar.getSpeed() != null && myRaceCar.getSpeed() > maxSpeed)
      asetaPaluuArvo("KAARRE_LIIKANOPEUS_KAASU", KAARRE_LIIKANOPEUS2_KAASU, Weight.SPEED);
    else if (myRaceCar.getSpeed() != null && myRaceCar.getSpeed() > KAARRE_LIIKANOPEUS1)
      asetaPaluuArvo("KAARRE_LIIKANOPEUS_KAASU", KAARRE_LIIKANOPEUS1_KAASU, Weight.SPEED);

    else if (currentAcceleration > 0.10)
    {
      asetaPaluuArvo("currentAcceleration > 0.10", 0.0, Weight.ANGLE);
    } else if (currentAcceleration > 0.05)
      asetaPaluuArvo("currentAcceleration > 0.05", 0.2, Weight.ANGLE);
    else if (currentAcceleration > 0.01) {
      double value = prevThrottleValue - 0.2 > 0 ? prevThrottleValue - 0.2 : 0.0;
      asetaPaluuArvo("currentAcceleration > 0.01", value, Weight.ANGLE);
    } else if (currentAcceleration > 0.00)
      asetaPaluuArvo("currentAcceleration > -0.01", prevThrottleValue, Weight.ANGLE);
    else if (currentAcceleration > -0.05) {
      double value = prevThrottleValue + 0.2 < 1.0 ? prevThrottleValue + 0.2 : 1.0;
      asetaPaluuArvo("currentAcceleration > -0.05", value, Weight.ANGLE);
    } else if (currentAcceleration > -0.10)
      asetaPaluuArvo("currentAcceleration > -0.10", 0.8, Weight.ANGLE);
    else
      asetaPaluuArvo("currentAcceleration <= -0.10", 1.0, Weight.ANGLE);

    return palautaEdotettuArvo();
  }

  private Double considerCurved() throws Exception {

    if (myRaceCar.getSpeed() != null && myRaceCar.getSpeed() > KAARRE_LIIKANOPEUS2)
      asetaPaluuArvo("KAARRE_LIIKANOPEUS_KAASU", KAARRE_LIIKANOPEUS2_KAASU, Weight.SPEED);
    else if (myRaceCar.getSpeed() != null && myRaceCar.getSpeed() > KAARRE_LIIKANOPEUS1)
      asetaPaluuArvo("KAARRE_LIIKANOPEUS_KAASU", KAARRE_LIIKANOPEUS1_KAASU, Weight.SPEED);

    double angle = Math.abs(myRaceCar.getAngle());
    // Go slower if the angle too great
    if (angle > KAARREKULMA1)
      asetaPaluuArvo("KAARREKULMA1_KAASU", KAARREKULMA1_KAASU, Weight.ANGLE);
    else if (angle > KAARREKULMA2)
      asetaPaluuArvo("KAARREKULMA2_KAASU", KAARREKULMA2_KAASU, Weight.ANGLE);
    else if (angle > KAARREKULMA3)
      asetaPaluuArvo("KAARREKULMA3_KAASU", KAARREKULMA3_KAASU, Weight.ANGLE);
    else if (angle > KAARREKULMA4)
      asetaPaluuArvo("KAARREKULMA4_KAASU", KAARREKULMA4_KAASU, Weight.ANGLE);
    else
      asetaPaluuArvo("KAARRE_KAASU", KAARRE_KAASU, Weight.ANGLE);

    return palautaEdotettuArvo();
  }

  public CarPositionsWrapper getCarPositions() throws Exception {
    return carPositions;

  }

  public RaceCars getRaceCars() throws Exception {
    return main.getRaceCars();
  }

  public void youCrashed() throws Exception {
    offTrack = true;
  }

  public void youContinue() throws Exception {
    offTrack = false;
  }
}

package noobbot.ai;

public class Physics {

  private static Double coeficient = 1.53; // 1.53, 1.7

  public static Double getFrictionCoeficient() {
    return coeficient;
  }

  public static void setFrictionCoeficient(Double coeficient) {
    Physics.coeficient = coeficient;
  }

  Double coeficient100 = 1.0; // 1.12892;
  Double coeficient50 = 1.0; // 0.8

  public Double maxCentrifugalF;

  private final Double SPEED_VAKIO = 6.37;
  private final Double ANGLE_DIFF_VAKIO = 0.35647142356;

  private Double maxSpeedAtRadius = 6.48;
  private int maxSpeedRadius = 90;
  private Double dragConstant;
  private Double friction = 0.02; // Start value needs to be enough to start sliding but small
  // enough not to start sliding too much in order to get an accurate calculated friction constant

  public void setMaxSpeedAtRadius(Double speed, int radius)
  {
    this.maxSpeedAtRadius = speed;
    this.maxSpeedRadius = radius;
  }

  public Double getCENTRIPETAL_ACCELERATION() {
    return Math.pow(maxSpeedAtRadius, 2) / maxSpeedRadius;
  }

  /**
   * My lame algorithm that is based on constant values for a successful race. Worked though except
   * when the expected max speed in the curve when over 7, then I crashed.
   * 
   * @param curSpeed
   * @param curAngleDiff
   * @param radius
   * @return
   */
  private void setNewMax(Double curSpeed, Double curAngleDiff, int radius) throws Exception
  {
    if (curSpeed == null || curSpeed == 0 || curAngleDiff == null || curAngleDiff == 0.0)
      return;

    Double angleDiffCalc = curAngleDiff * (SPEED_VAKIO / curSpeed);
    Double angleDiffCalcDiff = angleDiffCalc - ANGLE_DIFF_VAKIO;
    Double newMax = (1 - (angleDiffCalcDiff / ANGLE_DIFF_VAKIO)) * maxSpeedAtRadius;
    Double speedAtThisradius = newMax * (radius / maxSpeedRadius);

    maxSpeedRadius = radius;

    this.maxSpeedAtRadius = speedAtThisradius;


  }

  public void adjustMaxSpeed(Double curSpeed, Double curAngle, int radius) throws Exception
  {
    // setMaxCentrifugalF(curSpeed, curAngle, radius);

    setNewMax(curSpeed, curAngle, radius);
  }

  /**
   * Algorith received by maverikou. The car goes slow.
   * 
   * @param curSpeed
   * @param curAngle
   * @param radius
   * @return
   */
  private void setMaxCentrifugalF(Double curSpeed, Double curAngle, int radius) throws Exception
  {
    if (curAngle == 0.0)
      return;
    Double angV = Math.toDegrees(curSpeed / radius);

    Double angVmax = angV - curAngle;

    Double maxSpeed = Math.toRadians(angVmax) * radius;

    maxCentrifugalF = maxSpeed * maxSpeed / radius;

  }

  public void adjustMaxCentrifugalF(Double value) throws Exception
  {
    coeficient100 = coeficient100 * value;
    coeficient50 = coeficient50 * value;
  }

  /**
   * Calculates the maximum speed in the curve.
   * 
   * @return
   */
  public Double getMaximumSpeed(int r) throws Exception
  {
    // Double coeficient = (r >= 90 ? coeficient100 : coeficient50);
    // return returnMaxBasedOnMaverikou(r) * coeficient;

    if (friction == null)
      return returnMaxBasedOnMe(r);
    else
      return maxBasedOnFriction(r);
  }

  private Double maxBasedOnFriction(int r) {

    Double calFriction = friction * coeficient;
    double val = Math.sqrt(calFriction * r * 9.8);
    return val;
  }

  private Double returnMaxBasedOnMe(int r) throws Exception {

    double sqrt = Math.sqrt(r * getCENTRIPETAL_ACCELERATION());

    return sqrt;
  }

  private Double returnMaxBasedOnMaverikou(int r) throws Exception {
    if (maxCentrifugalF == null)
      return 6.48;

    double sqrt = Math.sqrt(r * maxCentrifugalF);

    return sqrt;
  }


  /**
   * Calculate drag constant at 0.0 throttle
   * 
   * @param v1
   *          speed at time t
   * @param v2
   *          speed at time t+1
   * @return
   */
  public void setDragConstant(Double v1, Double v2) throws Exception
  {
    dragConstant = Math.log(v2 / v1); // Natural logarithm
  }

  /**
   * Calculate needed distance to decelerate to the given target speed from
   * the given start (v0) speed
   * 
   * @param vTarget
   *          - speed to reach (decelerate)
   * @param v0
   *          - original speed
   * @return
   */
  public Double distanceToSpeed(Double vTarget, Double v0) throws Exception
  {
    if (dragConstant == null)
      return null;

    // Calculate the time needed to reach the target speed by deceleration
    Double time = (1 / dragConstant) * Math.log(vTarget / v0); // natural logarithm

    // Add up all the speeds during the time to get the distance (v(1)+v(2)+..+v(time) (target speed))
    // v(t) = v(0) * e^(t*k); k = dragConstant
    Double x = 0.0;
    for (int i = 1; i <= time; i++)
    {
      x = x + Math.exp(dragConstant * i);
    }
    return v0 * x;
  }

  public void setFriction(Double maxSpeedInThisCurve, int r) throws Exception {
    friction = frictionFormula(maxSpeedInThisCurve, r);

    // if (friction < 0.02)
    // friction = 0.02;

    maxSpeedRadius = r;

    this.maxSpeedAtRadius = maxSpeedInThisCurve;

  }

  private Double frictionFormula(Double maxSpeedInThisCurve, int r) throws Exception {
    return (maxSpeedInThisCurve * maxSpeedInThisCurve) / (r * 9.8);
  }

  public Double getFriction() {
    return friction;
  }

  public void adjustFrictionValue(Double crippledCoeficient) throws Exception {
    coeficient = coeficient * crippledCoeficient;

  }

  public void setFrictionIfGreater(Double speed, int laneRadius) throws Exception {

    Double newFriction = frictionFormula(speed, laneRadius);
    if (newFriction > friction)
      friction = newFriction;

  }

  public Double getDragConst() throws Exception {
    return dragConstant;
  }

}

package noobbot.ai;

import java.util.List;

import noobbot.logging.LogLevel;
import noobbot.logging.Logger;
import noobbot.logging.RaceInfo;
import noobbot.wrappers.CarWrapper;
import noobbot.wrappers.TrackWrapper;

public class LaneAdvicer {

  private Driver driver;
  private MyRaceCar myRaceCar;
  private CarWrapper carInfo;
  private TrackWrapper track;
  private RaceInfo raceInfo;

  private int lastPieceIndex;
  private int switchIndex = 1;

  public LaneAdvicer(Driver driver, TrackWrapper track, RaceInfo raceInfo)
  {
    this.driver = driver;
    myRaceCar = driver.myRaceCar;
    this.track = track;
    this.raceInfo = raceInfo;
  }

  public int considerLaneChangeAdvanced() throws Exception
  {
    if (!isNowTimeForDecision())
      return TrackWrapper.NONE;

    RaceCars cars = driver.getRaceCars();
    int pieceIndex = myRaceCar.getPieceIndex();
    List<Integer> trackSection = track.getTrackUntilNextSwitch(pieceIndex);

    int myLane = myRaceCar.getLane().getEndLaneIndex();
    List<LaneOption> laneOptions = track.getLaneOptions(myLane, pieceIndex);

    int value = 0;
    Integer bestLane = null;
    int prevValue;
    LaneOption prevOption = null;

    for (LaneOption option: laneOptions)
    {
      option.carsOnLane = cars.getCarsOnLane(trackSection, option.laneIndex);

      prevValue = value;
      value = calculateLaneOption(option);

      if (bestLane == null || value < prevValue || (value == prevValue && option.laneLengthOrder < prevOption.laneLengthOrder))
      {
        bestLane = option.laneIndex;
      }
      prevOption = option;
      String logStr = "P" + pieceIndex + "ML" + myLane + " L" + option.laneIndex + " C" + option.carsOnLane.size() + " V" + value + " B"
          + bestLane;
      Logger.getInstance().log(LogLevel.AI, logStr, raceInfo);
    }

    if (bestLane > myLane)
      return TrackWrapper.RIGHT;
    if (bestLane < myLane)
      return TrackWrapper.LEFT;

    // no lane change recommended
    return TrackWrapper.NONE;
  }

  private boolean isNowTimeForDecision() throws Exception {
    if (!myRaceCar.getPiece().isNextLaneSwitchable())
      return false;
    if (myRaceCar.getPiece().isSwitchSent())
      return false;

    // Make the decision in the last minute. This would be the last tick before the switch piece
    Double distToNextPiece = myRaceCar.getDistanceToNextPiece();
    if (distToNextPiece > myRaceCar.getSpeed())
      return false;

    return true;
  }

  /**
   * Calculate a value for determining how good path this lane is.
   * 0 = empty lane
   * 1 = fast lane
   * 2 = slow lane
   * 
   * @return
   */
  private int calculateLaneOption(LaneOption option) throws Exception {

    int carCount = option.carsOnLane.size();
    if (carCount == 0)
      return 0; // emptylane

    boolean slowlane = false;

    for (RaceCar car: option.carsOnLane)
    {
      int lap = car.getLap();
      Double distance = myRaceCar.getDistanceToCar(car);

      // If the car is behind me in laps then it is a slow car
      if (!slowlane)
        slowlane = lap < myRaceCar.getlap();

      // if the car is close then it is slow
      if (!slowlane)
        slowlane = distance > 0 && distance < 50;

      // Check the speeds
      Double carSpeed = car.getSpeed();
      Double mySpeed = myRaceCar.getSpeed();

      if (carSpeed < mySpeed)
      {

        // Calculate the time to reach the car
        Double speedDiff = mySpeed - carSpeed;
        Double timeToReachTheCar = distance / speedDiff;

        // if the car cannot get to the next switch before we can reach it then it is slow
        Double distanceToSwitch = car.getDistanceToNextSwitchPiece();
        Double timeCarToReachNextSwitch = distanceToSwitch / carSpeed;
        if (!slowlane && timeCarToReachNextSwitch != Double.POSITIVE_INFINITY) // sometimes the timeCarToReachNextSwitch is calculated to
                                                                               // infinity for some reason
          slowlane = ((distance > 0) && (timeCarToReachNextSwitch > timeToReachTheCar));

        Logger.getInstance().log(
            LogLevel.AI,
            "SwitchT" + String.format("%.2f", timeCarToReachNextSwitch) + " ReachT" + String.format("%.2f", timeToReachTheCar) + "D "
                + distance, raceInfo);

      }
      else
      {
        Logger.getInstance().log(LogLevel.AI, "MS" + mySpeed + " S" + carSpeed, raceInfo);
      }

    }

    if (slowlane)
      return 2; // slowlane
    else
      return 0; // fastlane (was 1)

    // LOGIC:
    // shortestLane + fastlane (9) > emptylane (8) ==> do not try to pass faster cars on the outer lane (??)
    // emptylane (8) > shortestLane + slowlane (3) ==> empty outer lane should be good for overtaking slow traffic on the inner lane
    // fastlane (4) > shortestLane + slowlane (3) ==> faster traffic on the outer lane should be good for overtaking slow traffic on the
    // inner
    // lane
    // emptylane > fastlane > slowlane

    // if no cars on lane add 8 to value
    // int emptylaneValue = 8;
    // int evenShortersLane = 6;
    // int shortestLane = 5;
    // int slowlaneValue = -2;
    // int fastlaneValue = 4;
    //
    // value = value + carCount == 0 ? emptylaneValue : 0;
    //
    // if (carCount != 0)
    // {
    // value = value + (slowlane ? slowlaneValue : fastlaneValue);
    // }
    //
    // return value;
  }

  public int considerLaneChange() throws Exception {

    isNowTimeForDecision();
    if (myRaceCar.isNextOnInnerlane())
      return TrackWrapper.NONE;

    return myRaceCar.moveTowardsInnerlane();
  }

}

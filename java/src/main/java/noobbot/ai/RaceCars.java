package noobbot.ai;

import java.util.ArrayList;
import java.util.List;

import noobbot.logging.RaceInfo;
import noobbot.wrappers.CarPositionsWrapper;
import noobbot.wrappers.CarWrapper;
import noobbot.wrappers.PieceWrapper;
import noobbot.wrappers.TrackWrapper;

public class RaceCars {

  public RaceCars() {
  }

  public List<RaceCar> getCars() throws Exception {
    return cars;
  }

  private List<RaceCar> cars;
  private TrackWrapper track;
  private RaceInfo raceInfo;

  public void updateRaceCarPositions(CarPositionsWrapper carPositions) throws Exception {

    if (cars == null)
    {
      cars = new ArrayList<RaceCar>();
      for (CarWrapper car: carPositions.getCars())
      {
        RaceCar raceCar = new RaceCar();
        raceCar.setCarInfo(car, carPositions);
        raceCar.setTrackInfo(track);
        cars.add(raceCar);
      }
    }
    else
    {
      for (CarWrapper car: carPositions.getCars())
      {
        RaceCar foundRaceCar = null;

        for (RaceCar raceCar: cars)
        {
          if (raceCar.getName().equals(car.getName()))
          {
            foundRaceCar = raceCar;
            break;
          }

        }
        if (foundRaceCar != null)
          foundRaceCar.setCarInfo(car, carPositions);
      }
    }
  }

  public void setTrackInfo(TrackWrapper track, RaceInfo raceInfo) throws Exception {
    this.track = track;
    this.raceInfo = raceInfo;

  }

  public RaceCar getMyCar() throws Exception
  {
    for (RaceCar car: getCars())
    {
      if (car.getName().equals(raceInfo.raceName))
      {
        return car;
      }
    }
    return null;
  }

  /**
   * Look for a car within the given distance from my car on the same lane. Return the car
   * closest to my car. Return null if no cars found.
   * 
   * @param dist
   * @param laneIndex
   * @return
   */
  public RaceCar getClosestCarOnLane(Double distance) throws Exception
  {
    MyRaceCar myCar = (MyRaceCar)getMyCar();
    int laneIndex = myCar.getLane().getEndLaneIndex();
    PieceWrapper piece = myCar.getPiece();

    int pieceIndex = myCar.getPieceIndex();
    Double length = piece.getLaneLenght(laneIndex) - myCar.getInPieceDistance();
    List<Integer> trackSection = new ArrayList<Integer>();
    while (length < distance)
    {
      trackSection.add(pieceIndex++);
      piece = piece.getNextPiece();
      if (piece.isSwitchable())
        // stop looking when switchable piece is found
        break;
      length = length + piece.getLaneLenght(laneIndex);
    }

    List<RaceCar> carsOnLane = getCarsOnLane(trackSection, laneIndex);
    Double closestDistance = null;
    RaceCar closestCar = null;
    for (RaceCar car: carsOnLane)
    {
      Double dist = myCar.getDistanceToCar(car);
      if (closestDistance == null || dist < closestDistance)
      {
        closestDistance = dist;
        closestCar = car;
      }
    }

    return closestCar;
  }

  /**
   * Get all car currently on given track section and given lane
   * 
   * @param trackSection
   * @param laneIndex
   * @return
   */

  public List<RaceCar> getCarsOnLane(List<Integer> trackSection, int laneIndex) throws Exception
  {
    List<RaceCar> allCars = getCarsOnSection(trackSection);
    List<RaceCar> carsOnLane = new ArrayList<RaceCar>();

    for (RaceCar car: allCars)
    {
      if (car.getLane().getEndLaneIndex() == laneIndex && !car.getName().equals(raceInfo.raceName))
      {
        // Add cars on same lane but not own car
        carsOnLane.add(car);
      }
    }

    return carsOnLane;
  }

  /**
   * Get all cars on the given track section
   * 
   * @param trackSection
   * @return
   */
  private List<RaceCar> getCarsOnSection(List<Integer> trackSection) throws Exception
  {
    List<RaceCar> list = new ArrayList<RaceCar>();
    for (RaceCar car: getCars())
    {
      if (trackSection.indexOf(car.getPieceIndex()) != -1)
      {
        // The car is located inside the trackSection
        list.add(car);
      }
    }

    return list;
  }

}

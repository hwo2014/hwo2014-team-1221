package noobbot.ai;

import java.util.List;

public class LaneOption {

  public int laneIndex;
  public Double length;
  public List<RaceCar> carsOnLane;
  public int laneLengthOrder = 2; // 0==shortest length

  public LaneOption(int lane, Double laneLength) {
    this.laneIndex = lane;
    this.length = laneLength;
  }

}

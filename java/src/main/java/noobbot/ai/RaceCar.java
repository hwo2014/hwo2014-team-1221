package noobbot.ai;

import noobbot.hwo.carpositions.Lane;
import noobbot.wrappers.CarPositionsWrapper;
import noobbot.wrappers.CarWrapper;
import noobbot.wrappers.PieceWrapper;
import noobbot.wrappers.TrackWrapper;

public class RaceCar {

  protected CarWrapper carInfo;
  private CarWrapper prevCarInfo;
  private TrackWrapper track;
  private Double prevSpeed;
  protected CarPositionsWrapper carPositions;
  private int currentTick;

  public RaceCar() {
    super();
  }

  public void setTrackInfo(TrackWrapper track) {
    this.track = track;
  }

  public void setCarInfo(CarWrapper carInfo, CarPositionsWrapper carPositions) throws Exception {
    if (prevCarInfo != null && this.carInfo != null)
    {
      prevSpeed = getSpeed();
    }
    prevCarInfo = this.carInfo;
    this.carInfo = carInfo;
    this.carPositions = carPositions;
    if (carPositions.getGameTick() != null)
      this.currentTick = carPositions.getGameTick();
  
  }

  /**
   * Calculate speed.
   * 
   * @return
   */
  public Double getSpeed() throws Exception {
    if (prevCarInfo == null)
      return 0.0;
  
    if (prevCarInfo.getPieceIndex() != carInfo.getPieceIndex())
    {
      final Double prevLaneLength = determineTravelledDistanceInPreviousPiece();
      if (prevLaneLength == null)
      {
        // Could not determine the length and there fore cannot determine the speed either
        return null;
      }
      return (prevLaneLength - prevCarInfo.getInPieceDistance()) + carInfo.getInPieceDistance();
    }
    else
    {
      return carInfo.getInPieceDistance() - prevCarInfo.getInPieceDistance();
    }
  
  }

  /**
   * get the length of the previous piece. Returns null if the previous
   * piece was a curved switch piece and switching was going on. TODO calculate this.
   * 
   * @return
   */
  private Double determineTravelledDistanceInPreviousPiece() throws Exception {
  
    PieceWrapper prevPiece = carInfo.getPiece().getPrevPiece();
  
    // get the start and end lane in the previous carPosition (previous piece)
    int endLaneIndex = prevCarInfo.getLane().getEndLaneIndex();
    int startLaneIndex = prevCarInfo.getLane().getStartLaneIndex();
    if (startLaneIndex != endLaneIndex)
    {
      // If switching is going on?
      int laneDist = track.getLaneDistance(startLaneIndex, endLaneIndex);

      if (prevCarInfo.getPiece().getInnerLaneSide() == TrackWrapper.RIGHT)
      {
        laneDist = laneDist * (-1);
      }

      return prevPiece.getSwitchLaneLenght(endLaneIndex, laneDist);
    }
    return prevPiece.getLaneLenght(endLaneIndex);
  }

  public String getSpeedStr(int decimalplaces) throws Exception {
    String format = "%." + decimalplaces + "f";
    String ret = String.format(format, getSpeed());

    return ret.replace(",", ".");
  }

  /**
   * Get the speed in the previous tick
   * 
   * @return
   */
  public Double getPrevCarSpeed() throws Exception {
    return prevSpeed;
  }

  public double getPrevCarAngleDiff() throws Exception {
    if (prevCarInfo == null)
      return 0;
    double diff = Math.abs(carInfo.getAngle() - prevCarInfo.getAngle());
    if (carInfo.getAngle() < prevCarInfo.getAngle())
    {
      // angle is decreasing
      diff = diff * (-1);
    }
    return diff;
  }

  /**
   * can be null
   * 
   * @return
   */
  public Double getAcceleration() throws Exception {
    Double s = getSpeed();
    Double p = getPrevCarSpeed();
    if (s == null || p == null)
      return null;
  
    return s - p;
  }

  public PieceWrapper getPiece() throws Exception {
    return carInfo.getPiece();
  }

  public Integer getPieceIndex() throws Exception {
  
    return carInfo.getPieceIndex();
  }

  public Lane getLane() throws Exception {
    return carInfo.getLane();
  }

  public Double getAngle() throws Exception {
    return carInfo.getAngle();
  }

  public Double getInPieceDistance() throws Exception {
    return carInfo.getInPieceDistance();
  }

  public int getlap() throws Exception {
  
    return carInfo.getLap();
  }

  public String getName() throws Exception {
    return carInfo.getName();
  }

  public Double getDistanceToCar(RaceCar car) throws Exception {
    return carInfo.getDistanceTo(car.carInfo);
  }

  public Double getDistanceToNextPiece() throws Exception {
    return getPiece().getLaneLenght(getLane().getEndLaneIndex()) - getInPieceDistance();
  }

  public int getLap() throws Exception {
    return carInfo.getLap();
  }

  /**
   * Get the distance from current piece location to the start of next switch piece
   * 
   * @return
   */
  public Double getDistanceToNextSwitchPiece() throws Exception {

    return getPiece().getDistanceToNextSwitchPiece(getLane().getEndLaneIndex()) - getInPieceDistance();
  }

}
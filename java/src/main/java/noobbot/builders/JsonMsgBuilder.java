package noobbot.builders;

import noobbot.hwo.createrace.CreateRace;
import noobbot.hwo.joinrace.JoinRace;
import noobbot.hwo.turbo.Turbo;

public class JsonMsgBuilder {

  public static CreateRace getCreateRaceMsg(Integer carCount, String botId, String botName)
  {
    return getCreateRaceMsg(carCount, null, null, botId, botName);
  }

  public static CreateRace getCreateRaceMsg(Integer carCount, String trackName, String password, String botId, String botName)
  {
    noobbot.hwo.createrace.BotId id = new noobbot.hwo.createrace.BotId();
    id.setKey(botId);
    id.setName(botName);

    noobbot.hwo.createrace.Data data = new noobbot.hwo.createrace.Data();
    data.setBotId(id);
    data.setCarCount(carCount);
    if (trackName != null && password != null)
    {
      data.setTrackName(trackName);
      data.setPassword(password);
    }
    CreateRace msg = new CreateRace();
    msg.setMsgType("CreateRace");
    msg.setData(data);

    return msg;
  }

  public static JoinRace getJoinRaceMsg(Integer carCount, String botId, String botName)
  {
    return getJoinRaceMsg(carCount, null, null, botId, botName);
  }

  public static JoinRace getJoinRaceMsg(Integer carCount, String trackName, String botId, String botName)
  {
    return getJoinRaceMsg(carCount, trackName, null, botId, botName);
  }

  public static JoinRace getJoinRaceMsg(Integer carCount, String trackName, String password, String botId, String botName)
  {
    noobbot.hwo.joinrace.BotId id = new noobbot.hwo.joinrace.BotId();
    id.setKey(botId);
    id.setName(botName);

    noobbot.hwo.joinrace.Data data = new noobbot.hwo.joinrace.Data();
    data.setBotId(id);
    data.setCarCount(carCount);
    if (trackName != null)
    {
      data.setTrackName(trackName);
    }
    if (password != null)
    {
      data.setPassword(password);
    }

    JoinRace msg = new JoinRace();
    msg.setMsgType("joinRace");
    msg.setData(data);

    return msg;

  }

  public static Turbo getTurboMsg() {
    Turbo turbo = new Turbo();
    turbo.setMsgType("turbo");
    turbo.setData("Let's go");
    return turbo;
  }

}

/**
 * 
 */
package noobbot.logging;

/**
 * @author tup
 *
 */
public enum DashCauge {

  SPEED,
  LAP,
  POS, FRICTCONST, DRAG, FRICTCOEF, THROTTLE, ANGLE
}

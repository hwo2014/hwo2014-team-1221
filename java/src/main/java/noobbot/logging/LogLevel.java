/**
 * 
 */
package noobbot.logging;

/**
 * @author tup
 *
 */
public enum LogLevel {

  LOG1,
  LOG2,
  LOG3,
  ACTIONRULE_USED,
  GAMERESULTS,
  PROGRESS,
  MESSAGE,
  CARPOS_MESSAGE,
  AI,
  CURVEANGLE,
  DETAIL, CRASH, CARPOSITIONS

}

package noobbot.logging;

import java.text.SimpleDateFormat;
import java.util.Date;

public class RaceInfo {

  public String testHost;
  public String TrackName;
  public int numberOfCars;
  public String raceName;
  public String date;
  public boolean crippled;
  public Double crippledCoeficient;
  public String password;

  public RaceInfo(String testHost, String trackName, int number, String raceName, boolean b, Double d, String psw)
  {
    this.testHost = testHost;
    this.TrackName = trackName;
    this.numberOfCars = number;
    this.raceName = raceName;
    this.crippled = b;
    this.crippledCoeficient = d;

    Date date = new Date();
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm");
    String dateStr = dateFormat.format(date);
    this.date = dateStr;
    this.password = psw;
  }

}

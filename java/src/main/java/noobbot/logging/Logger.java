package noobbot.logging;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import noobbot.Main;
import noobbot.ai.MyRaceCar;
import noobbot.ai.Physics;
import noobbot.hwo.crash.Crash;
import noobbot.hwo.dnf.Data;
import noobbot.wrappers.CarPositionsWrapper;
import noobbot.wrappers.CarWrapper;

public class Logger {

  private File raceCarLogFile = new File("RaceCarLog.log");

  private boolean hasCrashed = false;

  private static Logger instance;

  private MyRaceCar myRaceCar;
  private Main main;
  private FileOutputStream actionsFile;
  private FileOutputStream carposFile;
  private FileOutputStream messageFile;
  private FileOutputStream progressFile;
  private FileOutputStream curveAngleFile;

  private RaceInfo raceInfo;

  private boolean logOnlyToConsole = false;

  private List<CarWrapper> prevPositions;

  private int i;

  private Logger()
  {
  }

  public static Logger getInstance()
  {
    if (instance == null)
    {
      instance = new Logger();
    }

    return instance;
  }

  public void closeLogger() throws Exception
  {
    System.out.println("CRASHED:" + hasCrashed);

    if (logOnlyToConsole)
      return;

    if (main.gui == null)
    {
      return;
    }

    try {

      logRaceCarHistory();

      actionsFile.close();
      carposFile.close();
      messageFile.close();
      progressFile.close();
      curveAngleFile.close();

    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  private void createOutputStreams() throws Exception {
    if (main.gui == null)
    {
      return;
    }

    try {
      File afile = buildFileName("actions", raceInfo);
      actionsFile = new FileOutputStream(afile);
      File cfile = buildFileName("carPos", raceInfo);
      carposFile = new FileOutputStream(cfile);
      File mfile = buildFileName("message", raceInfo);
      messageFile = new FileOutputStream(mfile);
      File pfile = buildFileName("progress", raceInfo);
      progressFile = new FileOutputStream(pfile);
      File cafile = buildFileName("curveAngle", raceInfo);
      curveAngleFile = new FileOutputStream(cafile);

      String headings = "TICK, SPEED, MAXSPEED, CENTRIFUGAL, curCF, (RADIUS), ANGLE, ANGLEDELTA, (FRICTION)";
      writeToFile(headings, curveAngleFile);

    } catch (FileNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  private void logRaceCarHistory() throws Exception {

    Map<String, RaceHistory> raceHistory = new HashMap<String, RaceHistory>();

    for (CarWrapper c: myRaceCar.getRaceHistory())
    {
      RaceHistory car = raceHistory.get(c.getRecordedAction());
      if (car == null)
      {
        RaceHistory history = new RaceHistory();
        history.count = 1;
        history.position = "??";
        history.throttle = c.getRecordedThrottle();
        raceHistory.put(c.getRecordedAction(), history);
      }
      else
      {
        car.count++;
      }
    }

    try {
      FileOutputStream out = new FileOutputStream(raceCarLogFile);

      String content;
      for (String key: raceHistory.keySet())
      {
        RaceHistory history = raceHistory.get(key);
        content = key + ":" + history.count + " \n";
        out.write(content.getBytes());
        writeNewLineToStream(out);
      }
      content = "Total count: " + myRaceCar.getRaceHistory().size();
      out.write(content.getBytes());
      writeNewLineToStream(out);
      out.close();
    } catch (Throwable e) {
      // D.error(e);
    }// try

  }

  private void writeNewLineToStream(FileOutputStream out) throws IOException {
    out.write(13);
    out.write(10);
  }

  private void writeToFile(String content, File file)
  {
    try {
      FileOutputStream out = new FileOutputStream(file);
      out.write(content.getBytes());
      out.close();
    } catch (Throwable e) {
      // D.error(e);
    }// try
  }

  private void writeToFile(String content, FileOutputStream out)
  {
    // do nothing if logging is only to be done to console
    if (logOnlyToConsole)
      return;

    try {
      out.write(content.getBytes());
      writeNewLineToStream(out);
    } catch (Throwable e) {
      // D.error(e);
    }// try
  }

  private File buildFileName(String msg, RaceInfo info) throws Exception {
    String dir = "logs\\" + info.TrackName + "\\";

    String name = ""; // default empty
    if (info.numberOfCars > 1)
    {
      dir = dir + "Cars" + info.numberOfCars + "_" + info.date + "\\";
      name = info.raceName + "_";
    }
    if (info.testHost.equals("prost.helloworldopen.com"))
    {
      dir = dir + "prost" + "\\";
    }

    File dirFile = new File(dir);
    if (!dirFile.exists())
      dirFile.mkdirs();

    File file = new File(dirFile, msg + "_" + name + info.date + ".log");
    return file;
  }

  public void logGameInit(String line, RaceInfo info) throws Exception
  {
    File file = buildFileName("gameInit", info);
    writeToFile(line, file);
    log(LogLevel.PROGRESS, "Muodostetaan rata", info);
  }

  public void logGameEnd(String line) throws Exception
  {
    log(LogLevel.PROGRESS, "Kilpailu loppui (Race End)", main.getRaceInfo());

    String logStr = main.gameEnd.getResultStr();
    logStr = logStr + main.gameEnd.getBestLapStr();
    log(LogLevel.GAMERESULTS, logStr, main.getRaceInfo());
  }

  public void logDnf(String line) throws Exception {

    String msg = "Auto diskattiin. Syy: ?";
    Data data = main.dnf.getData();
    if (data.getCar() != null && data.getReason() != null)
    {
      String name = data.getCar().getName();
      String color = data.getCar().getColor();
      String reason = data.getReason();
      msg = name + "(" + color + ") diskattiin. Syy:" + reason + "\n";
    }
    log(LogLevel.PROGRESS, msg, main.getRaceInfo());

  }

  public void logFinish(String line) throws Exception {
    String name = main.finish.getData().getName();
    String color = main.finish.getData().getColor();
    String msg = name + "(" + color + ") saapui maaliin\n";
    log(LogLevel.PROGRESS, msg, main.getRaceInfo());

  }

  public void logSpawn(String line) throws Exception {
    String name = main.spawn.getData().getName();
    String color = main.spawn.getData().getColor();
    String msg = name + "(" + color + ") jatkaa matkaa\n";
    log(LogLevel.PROGRESS, msg, main.getRaceInfo());

  }

  public void logCrash(Crash crash) throws Exception {

    hasCrashed = true;

    String name = crash.getData().getName();
    int tick = crash.getGameTick();
    Double speed = myRaceCar.getSpeed();
    String logMsg = name + " CRASH; Tick=" + tick + ";speed=" + String.format("%.1f", speed);
    logMsg = logMsg + "angle:" + myRaceCar.getAngle() + "\n";

    log(LogLevel.PROGRESS, logMsg, main.getRaceInfo());
    log(LogLevel.ACTIONRULE_USED, logMsg, main.getRaceInfo());
  }

  public void logLapFinished(String line) throws Exception {

    Integer ms = main.lapFinished.getData().getLapTime().getMillis();
    Integer ticks = main.lapFinished.getData().getLapTime().getTicks();
    Integer lap = main.lapFinished.getData().getLapTime().getLap() + 1;
    Integer rankOverall = main.lapFinished.getData().getRanking().getOverall();
    Integer rankLap = main.lapFinished.getData().getRanking().getFastestLap();
    String msg = "L A P  F I N I S H E D (" + lap + ")\n";
    msg = msg + "LAP TIME: " + ms + "ms (ticks=" + ticks + "\n";
    msg = msg + "RANK OVERALL: " + rankOverall + "\n";
    msg = msg + "RANK LAP: " + rankLap + "\n";
    msg = msg + "\n\n\n";
    log(LogLevel.GAMERESULTS, msg, main.getRaceInfo());

  }

  public void logJson(String json, String filename) throws Exception {
    writeToFile(json, new File(filename));

  }

  public void setMyRaceCar(MyRaceCar myRaceCar) throws Exception {
    this.myRaceCar = myRaceCar;
  }

  public void setMain(Main main) throws Exception {
    this.main = main;
  }

  public void logGameTick() throws Exception {

    Integer tick = main.carPositions.getGameTick();
    if (tick == null || myRaceCar == null)
      return;
    Double speed = myRaceCar.getSpeed();
    int pieceIndex = myRaceCar.getPieceIndex();

    String msg = "Tick:" + tick + "s:" + speed + "I:" + pieceIndex;
    System.out.println(msg);

    logToGui(LogLevel.LOG1, msg, main.getRaceInfo());

  }

  private void logToGui(LogLevel level, String msg, RaceInfo info) throws Exception {

    if (main.gui == null)
    {
      return;
    }

    main.gui.log(level, msg, info);

  }

  public void logToGui(DashCauge cauge, String msg) throws Exception {

    if (main.gui == null)
    {
      return;
    }

    main.gui.logDashInfo(cauge, msg);

  }

  public void log(LogLevel level, String msg, RaceInfo info) throws Exception {

    // Send msg to GUI
    logToGui(level, msg, info);

    switch (level)
    {
      case ACTIONRULE_USED: {
        // Send msg to file
        writeToFile(msg, actionsFile); // does this slow it up
        break;
      }
      case GAMERESULTS: {
        // Send msg to console and file
        System.out.println(msg);
        writeToFile(msg, progressFile);
        break;
      }
      case PROGRESS: {
        // Send msg to console and file
        System.out.println(msg);
        writeToFile(msg, progressFile);
        break;
      }
      case CARPOS_MESSAGE: {
        // Send msg to file
        // writeToFile(msg, carposFile); does this slow it up
        break;
      }
      case MESSAGE: {
        // Send msg to file
        writeToFile(msg, messageFile);
        break;
      }
      case DETAIL: {
        // Send msg to console
        System.out.print(msg + ";");
        break;
      }
      case CURVEANGLE: {
        // Send msg to file
        // writeToFile(msg, curveAngleFile);
        break;
      }
      default:

    }

  }

  public void logUnknown(String line) throws Exception {
    String msg = "\n\nRECEIVED UNKOWN MSG:" + line + "\n\n";
    log(LogLevel.MESSAGE, msg, main.getRaceInfo());
    log(LogLevel.PROGRESS, msg, main.getRaceInfo());

  }

  public void logTurboAvailable(String line) throws Exception {
    Double ms = main.turboAvailable.getData().getTurboDurationMilliseconds();
    Integer ticks = main.turboAvailable.getData().getTurboDurationTicks();
    Double factor = main.turboAvailable.getData().getTurboFactor();
    String msg = "Turbo (factor=" + factor + ") available for " + ms + "ms (ticks=" + ticks + ")\n";
    log(LogLevel.PROGRESS, msg, main.getRaceInfo());
    log(LogLevel.ACTIONRULE_USED, msg, main.getRaceInfo());

  }

  public RaceInfo getRaceInfo() throws Exception {
    return raceInfo;
  }

  public void setRaceInfo(RaceInfo raceInfo) throws Exception {
    this.raceInfo = raceInfo;
    createOutputStreams();

  }

  public void setLoggingMode(boolean logonlytoconsole) throws Exception {
    this.logOnlyToConsole = logonlytoconsole;

  }

  public void logCarpositions(CarPositionsWrapper carPositions) throws Exception {

    i++;

    PosInfo[] carpos = new PosInfo[carPositions.getCars().size()];
    String msg = "";
    int carposindex = 0;
    for (CarWrapper car: carPositions.getCars())
    {
      String name = car.getName();
      int index = car.getPieceIndex();
      int lap = car.getLap();

      msg = msg + name + "(" + index + "/" + lap + ");";

      carpos[carposindex] = (new PosInfo(name, index, lap));
      carposindex++;
    }

    if (i % 30 == 0)
    {
      log(LogLevel.CARPOSITIONS, msg, this.raceInfo);
    }
    Arrays.sort(carpos);

    logDash(carpos);

  }



  private void logDash(PosInfo[] carpos) throws Exception {

    if (main.gui == null)
      return;

    Double speed = myRaceCar.getSpeed();
    Double targetSpeed = myRaceCar.getTargetSpeed();
    Double angle = myRaceCar.getAngle();

    main.gui.logDashInfo(DashCauge.SPEED, String.format("%.2f", speed) + "(" + String.format("%.2f", targetSpeed) + ")");
    main.gui.logDashInfo(DashCauge.ANGLE, String.format("%.2f", angle));

    int lap = myRaceCar.getlap();
    int piece = myRaceCar.getPieceIndex();
    int pos = Arrays.asList(carpos).indexOf(myRaceCar.getName()) + 1;
    main.gui.logDashInfo(DashCauge.LAP, piece + "/" + lap);
    main.gui.logDashInfo(DashCauge.POS, pos + "");

    Double frictionConst = main.gameInit.physics.getFriction();
    Double dragConst = main.gameInit.physics.getDragConst();
    Double frictionCoef = Physics.getFrictionCoeficient();

    main.gui.logDashInfo(DashCauge.FRICTCONST, String.format("%.5f", frictionConst));
    main.gui.logDashInfo(DashCauge.DRAG, String.format("%.5f", dragConst));
    main.gui.logDashInfo(DashCauge.FRICTCOEF, String.format("%.2f", frictionCoef));
  }

  class PosInfo implements Comparable<PosInfo>
  {
    public String name;
    public int index;
    public int lap;

    public PosInfo(String name, int i, int l)
    {
      this.name = name;
      this.index = i;
      this.lap = l;
    }

    @Override
    public int compareTo(PosInfo otherCarPos) {
      PosInfo A = this;
      PosInfo B = otherCarPos;
      if (A.lap < B.lap || (A.lap == B.lap && A.index < B.index))
      {
        return -1;
      }
      else
      {
        return 1;
      }
    }
  }

}

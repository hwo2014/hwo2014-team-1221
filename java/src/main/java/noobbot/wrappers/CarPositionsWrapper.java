package noobbot.wrappers;

import java.util.ArrayList;
import java.util.List;

import noobbot.hwo.carpositions.CarPositions;
import noobbot.hwo.carpositions.Datum;

import com.google.gson.Gson;

public class CarPositionsWrapper {

  private CarPositions positions;
  private GameInitWrapper gameInit;
  private List<CarWrapper> cars;

  final Gson gson = new Gson();
  public String myCarName;

  public CarPositionsWrapper(String line, GameInitWrapper gameInit, String carName)
  {
    this.positions = gson.fromJson(line, CarPositions.class);
    this.gameInit = gameInit;
    myCarName = carName;

  }

  public List<CarWrapper> getCars() throws Exception
  {
    if (cars == null)
    {
      cars = new ArrayList<CarWrapper>();
      for (Datum d: positions.getData())
      {
        CarWrapper car = new CarWrapper(d, gameInit.getTrack());
        cars.add(car);
      }

    }
    return cars;
  }

  public CarWrapper getMyCar() throws Exception
  {
    for (CarWrapper car: getCars())
    {
      if (car.getName().equals(myCarName))
      {
        return car;
      }
    }
    return null;
  }

  public Integer getGameTick() throws Exception
  {
    return positions.getGameTick();
  }

}

package noobbot.wrappers;

import java.util.List;

import noobbot.hwo.gameend.BestLap;
import noobbot.hwo.gameend.GameEnd;
import noobbot.hwo.gameend.Result;

import com.google.gson.Gson;

public class GameEndWrapper {

  private static GameEndWrapper instance;

  private GameEnd gameEnd;

  private Gson gson = new Gson();

  public static GameEndWrapper getInstance()
  {
    if (instance == null)
    {
      instance = new GameEndWrapper();
    }
    return instance;
  }

  public void fromJson(String json)
  {
    gameEnd = gson.fromJson(json, GameEnd.class);

  }

  public List<Result> getResults()
  {
    return gameEnd.getData().getResults();
  }

  public List<BestLap> getBestLaps()
  {
    return gameEnd.getData().getBestLaps();
  }

  public String getResultStr()
  {
    String result = "";
    int rank = 0;
    for (Result r:getResults())
    {
      rank++;
      result = result + "\nRANK " + rank + ": " + r.getCar().getName() + "(" + r.getCar().getColor() + ") ";
      result = result + "Time = " + r.getResult().getMillis() + "ms, Laps =" + r.getResult().getLaps();
      result = result + "\n";
    }
    return result;
  }

  public String getBestLapStr()
  {
    String result = "";
    int rank = 0;
    for (BestLap r: getBestLaps())
    {
      rank++;
      result = result + "\nRANK " + rank + ": " + r.getCar().getName() + "(" + r.getCar().getColor() + ") ";
      result = result + "Time = " + r.getResult().getMillis() + "ms";
      result = result + "\n";
    }
    return result;

  }

}

package noobbot.wrappers;

public class CarDimensionWrapper {

  public Double length;
  public Double width;
  public String name;

  public CarDimensionWrapper(String name, Double length, Double width)
  {
    this.length = length;
    this.width = width;
    this.name = name;
  }

}

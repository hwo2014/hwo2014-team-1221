package noobbot.wrappers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import noobbot.ai.LaneOption;
import noobbot.ai.Physics;
import noobbot.hwo.gameinit.Lane;
import noobbot.hwo.gameinit.Piece;
import noobbot.hwo.gameinit.Track;

public class TrackWrapper {

  public final static int LEFT = -1;
  public final static int RIGHT = -2;
  public final static int NONE = 0;

  private Track track;
  private AccidentInfo accidents;
  private List<PieceWrapper> piecelist;
  public Physics physics;

  public TrackWrapper(Track track, AccidentInfo accidents, Physics ph) {
    this.track = track;
    this.accidents = accidents;
    this.physics = ph;
  }

  public void setNewLap() throws Exception
  {
    for (PieceWrapper p: getPieces())
    {
      p.SetNewLap();
    }
  }

  public String getName() throws Exception
  {
    return track.getName();
  }

  public List<PieceWrapper> getPieces() throws Exception
  {
    if (piecelist == null)
    {
      piecelist = new ArrayList<PieceWrapper>();
      PieceWrapper prevPiece = null;
      PieceWrapper firstPiece = null;
      PieceWrapper lastPiece = null;
      int pieceIndex = 0;
      for (Piece p: track.getPieces())
      {
        PieceWrapper nextPiece = new PieceWrapper(p, prevPiece, this, physics, pieceIndex++);
        piecelist.add(nextPiece);
        if (prevPiece == null)
        {
          // This is the first piece in the track
          firstPiece = nextPiece;
          firstPiece.isFirstPiece = true;
        }
        else
        {
          prevPiece.setNextPiece(nextPiece);
        }
        prevPiece = nextPiece;
      }
      // This is the last piece
      lastPiece = prevPiece;
      lastPiece.isLastPiece = true;
      lastPiece.setNextPiece(firstPiece);
      firstPiece.setPrevPiece(lastPiece);
    }
    return piecelist;
  }

  public List<Lane> getLanes() throws Exception
  {
    return track.getLanes();
  }

  /***
   * Get PieceWrapper by index
   * 
   * @param index
   * @return
   */
  public PieceWrapper getPiece(int index) throws Exception
  {
    return getPieces().get(index);
  }

  /***
   * Determines the shortest path (inner lane)
   */
  public int getNextSwitchLane(int pieceIndex) throws Exception
  {
    int laneShift = NONE; // Do not shift lanes

    PieceWrapper currentPiece = getPiece(pieceIndex);

    Map shiftMap = new HashMap();
    for (PieceWrapper piece: getPieces())
    {
      shiftMap.put(piece, 1);
    }

    return laneShift;
  }

  public int getRightMostLaneIndex() throws Exception {
    return track.getLanes().size() - 1;
  }

  public int getLaneDistanceFromCenter(int laneIndex) throws Exception {

    // TODO assuming that lane index is the same as the lane number
    return getLanes().get(laneIndex).getDistanceFromCenter();
  }

  /**
   * Get the distance between two lanes
   * 
   * @param startLaneIndex
   * @param endLaneIndex
   * @return
   */
  public int getLaneDistance(int startLaneIndex, int endLaneIndex) throws Exception {
    int start = getLanes().get(startLaneIndex).getDistanceFromCenter();
    int end = getLanes().get(endLaneIndex).getDistanceFromCenter();


    return Math.abs(start - end);

  }

  /**
   * Determine the max speed in this and some of the following pieces
   * 
   * @param pieceIndex
   * @param laneIndex
   * @return
   */
  public Double getTrackMaxSpeed(Integer pieceIndex, int laneIndex, Double inPieceDistance, Double distance) throws Exception {

    Double lengthToBeMeasured = distance;
    Double lengthSoFar = 0 - inPieceDistance; // offset the start length with the current position
    int step = 0;
    Double maxSpeed = null;
    while (lengthToBeMeasured > lengthSoFar)
    {
      // Determine next piece index. Continue with index=0 if last piece
      int index = pieceIndex + step;
      if (index >= getPieces().size())
        index = 0;

      PieceWrapper piece = getPiece(index);
      Double max = piece.getMaximumSpeed(laneIndex, accidents); // returns null if straight piece

      if (maxSpeed == null || (max != null && max < maxSpeed))
        maxSpeed = max;

      // add piece length and go to the next piece
      lengthSoFar = lengthSoFar + piece.getLaneLenght(laneIndex);
      step++;
    }
    return maxSpeed;
  }

  /**
   * Returns a list of indexes of all pieces until a switch piece after the first one.
   * The latter switchable piece is not included
   * 
   * @param pieceIndex
   * @return
   */
  public List<Integer> getTrackUntilNextSwitch(int pieceIndex) throws Exception {

    List<Integer> list = new ArrayList<Integer>();
    PieceWrapper piece = getPiece(pieceIndex);
    list.add(pieceIndex); // Add the first index

    // The next piece is the first switch piece. Add that too.
    piece = piece.getNextPiece();
    list.add(pieceIndex + 1);
    // Now look for the next switch piece
    piece = piece.getNextPiece();
    // add indexes to list until switch piece is found
    while (!piece.isSwitchable())
    {

      list.add(getPieces().indexOf(piece));

      // Get the next piece
      piece = piece.getNextPiece();
    }
    return list;
  }

  private Double getLaneLengthFromSwitchToSwitch(int pieceIndex, int laneIndex) throws Exception {

    Double length = 0.0;

    // You cannot switch on the current piece
    PieceWrapper nextPiece = getPiece(pieceIndex).getNextPiece();
    // Find the first switchable piece
    while (!nextPiece.isSwitchable())
    {
      nextPiece = nextPiece.getNextPiece();
    }
    nextPiece = nextPiece.getNextPiece();
    while (!nextPiece.isSwitchable())
    {
      length = length + nextPiece.getLaneLenght(laneIndex);
      nextPiece = nextPiece.getNextPiece();
    }

    return length;
  }

  /**
   * Return a list of lanes that are to the left and right and current lane
   * 
   * @param myLane
   * @return
   */
  public List<LaneOption> getLaneOptions(int myLane, int pieceIndex) throws Exception {

    List<LaneOption> list = new ArrayList<LaneOption>();
    Double laneLength = getLaneLengthFromSwitchToSwitch(pieceIndex, myLane);
    list.add(new LaneOption(myLane, laneLength));
    int laneCount = getLanes().size();

    // is there a lane to the right?
    if (myLane + 1 < laneCount)
    {
      laneLength = getLaneLengthFromSwitchToSwitch(pieceIndex, myLane + 1);
      list.add(new LaneOption(myLane + 1, laneLength)); // yes there is!
    }
    // is there a lane to the left?
    if (myLane - 1 >= 0)
    {
      laneLength = getLaneLengthFromSwitchToSwitch(pieceIndex, myLane - 1);
      list.add(new LaneOption(myLane - 1, laneLength)); // yes there is!
    }

    // Set the lane length order
    list.get(0).laneLengthOrder = list.get(0).laneLengthOrder - (list.get(0).length < list.get(1).length ? 1 : 0);
    list.get(0).laneLengthOrder = list.get(0).laneLengthOrder - (list.size() == 3 && list.get(0).length < list.get(2).length ? 1 : 0);

    list.get(1).laneLengthOrder = list.get(1).laneLengthOrder - (list.get(1).length < list.get(0).length ? 1 : 0);
    list.get(1).laneLengthOrder = list.get(1).laneLengthOrder - (list.size() == 3 && list.get(1).length < list.get(2).length ? 1 : 0);

    if (list.size() == 3)
    {
      list.get(2).laneLengthOrder = list.get(2).laneLengthOrder + (list.get(2).length < list.get(0).length ? 1 : 0);
      list.get(2).laneLengthOrder = list.get(2).laneLengthOrder + (list.get(2).length < list.get(1).length ? 1 : 0);
    }

    return list;
  }

  /**
   * Calculate the length from start of the given piece(startIndex) to the START of the given piece(endIndex).
   * 
   * @param startIndex
   * @param endIndex
   * @param laneIndex
   * @return
   */
  public Double getDistanceFromStartOfPieceToStartOfPiece(int startIndex, int endIndex, int laneIndex) throws Exception {

    Double distance = 0.0;
    for (int i = startIndex; i < endIndex; i++)
    {
      distance = distance + getPieces().get(i).getLaneLenght(laneIndex);

    }
    return distance;
  }

  public void setDangerousCurveType(CurveType type) {

    accidents.addCrash(type);

  }
}

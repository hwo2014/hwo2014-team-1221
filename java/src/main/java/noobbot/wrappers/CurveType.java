package noobbot.wrappers;

public class CurveType {

  public int radius;
  public Double length;
  public int dir;
  public int crashes;

  public CurveType(int r, Double l, int d)
  {
    this.radius = r;
    this.length = l;
    this.dir = d;
  }

}

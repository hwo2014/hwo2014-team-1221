package noobbot.wrappers;

import noobbot.ai.Driver;
import noobbot.ai.Physics;
import noobbot.hwo.gameinit.Piece;

public class PieceWrapper {

  private Piece piece;
  private PieceWrapper edellinenPala;
  private PieceWrapper seuraavaPala;
  public boolean isFirstPiece;
  public boolean isLastPiece;
  private Integer innerLaneSide;
  private Integer straitsInFront = null;
  private TrackWrapper track;

  private int isSwitchSent = 0;

  private Double distanceToCurve = null;
  private Double maxSpeedOfNextCurvePiece;
  private Integer innerLaneIndex;
  private Physics physics;
  private int pieceIndex;

  private int crashesInthisCurve = 0;
  private CurveType completeType;

  public PieceWrapper(Piece p, PieceWrapper prevPiece, TrackWrapper track, Physics physics, int index) {
    this.piece = p;
    this.edellinenPala = prevPiece;
    this.track = track;
    this.physics = physics;
    this.pieceIndex = index;
  }

  public boolean isSwitchSent() throws Exception {
    return isSwitchSent != 0;
  }

  /**
   * Direction should be sent either LEFT or RIGHT
   * 
   * @param direction
   */
  public void setSwitchSent(int direction) throws Exception {
    this.isSwitchSent = direction;
  }

  public int getSwitchSent() throws Exception {
    return isSwitchSent;
  }

  public void SetNewLap() throws Exception
  {
    isSwitchSent = 0;
    maxSpeedOfNextCurvePiece = null;
  }

  public void setNextPiece(PieceWrapper nextPiece) throws Exception
  {
    this.seuraavaPala = nextPiece;
  }

  public PieceWrapper getNextPiece() throws Exception
  {
    return seuraavaPala;
  }

  public PieceWrapper getPrevPiece() throws Exception
  {
    return edellinenPala;
  }

  /**
   * Determines which lane is the inner lane. Jos pala ei sit� tied� se kysyy
   * sit� seuraavalta palalta.
   * 
   * @return
   */
  public int getInnerLaneSide() throws Exception
  {
    if (innerLaneSide == null)
    {
      if (isCurve())
      {
        innerLaneSide = curveDirection();
      }
      else
      {
        innerLaneSide = seuraavaPala.getInnerLaneSide();
      }
    }
    return innerLaneSide;
  }

  private Integer getInnerLaneIndex() throws Exception {
    if (innerLaneIndex == null)
    {
      if (isCurve())
      {
        if (curveDirection() == TrackWrapper.LEFT)
        {
          // Get the leftmost lane
          innerLaneIndex = track.getLanes().get(0).getIndex();
        }
        else
        {
          // TrackWrapper.RIGHT
          innerLaneIndex = track.getRightMostLaneIndex();
        }

      }
      else
      {
        innerLaneIndex = seuraavaPala.getInnerLaneIndex();
      }
    }
    return innerLaneIndex;
  }

  public int getStraightsInFront() throws Exception
  {
    if (straitsInFront == null)
    {
      if (getNextPiece().isCurve())
      {
        straitsInFront = 0;
      }
      else
      {
        straitsInFront = getNextPiece().getStraightsInFront() + 1;
      }
    }
    return straitsInFront;
  }

  /**
   * Get the length of the straight until a curve peace from the START of this peace
   * 
   * @return
   */
  public Double getDistanceToCurve() throws Exception {
    if (distanceToCurve == null)
    {
      if (isCurve())
      {
        distanceToCurve = 0.0; // getLength();
      }
      else
      {
        distanceToCurve = getNextPiece().getDistanceToCurve() + getLength();
      }
    }
    return distanceToCurve;
  }

  private int curveDirection() throws Exception {
    if (isCurve())
    {
      if (getAngle() > 0)
      {
        // right
        return TrackWrapper.RIGHT;
      }
      else
      {
        // left
        return TrackWrapper.LEFT;
      }
    }
    return TrackWrapper.NONE;

  }

  public void setInnerLane(int innerLane) throws Exception
  {
    this.innerLaneSide = innerLane;
  }

  public Double getLength() throws Exception
  {
    return piece.getLength();
  }

  public Integer getRadius() throws Exception
  {
    return piece.getRadius();
  }

  public Double getAngle() throws Exception
  {
    return piece.getAngle();
  }

  public Boolean get() throws Exception
  {
    return piece.getSwitch();
  }

  public boolean isStreight() throws Exception
  {
    return getLength() != null;
  }

  public boolean isCurve() throws Exception
  {
    return getRadius() != null;
  }

  public void setPrevPiece(PieceWrapper lastPiece) throws Exception {
    edellinenPala = lastPiece;

  }

  public boolean isSwitchable() throws Exception {

    Boolean s = piece.getSwitch();

    if (s == null)
      return false;

    return s;
  }

  public boolean isNextLaneSwitchable() throws Exception {

    Boolean s = seuraavaPala.piece.getSwitch();

    if (s == null)
      return false;

    return s;
  }

  public Double getLaneLenght(int lane) throws Exception {
    if (isStreight())
      return getLength();

    // is curve
    int radius = getLaneRadius(lane);
    Double angle = Math.abs(getAngle());

    Double circumference = 2 * Math.PI * radius * (angle / 360);

    return circumference;
  }

  public int getLaneRadius(int lane) throws Exception {
    int dist = track.getLaneDistanceFromCenter(lane);
    int radius;
    if (curveDirection() == TrackWrapper.RIGHT)
      radius = getRadius() - dist;
    else
      radius = getRadius() + dist;
    return radius;
  }

  public Double getSwitchLaneLenght(int lane, int laneDist) throws Exception {
    if (isStreight())
    {
      // Pythagoras
      Double x = Math.pow(laneDist, 2);
      Double y = Math.pow(getLength(), 2);
      return Math.sqrt(x + y);
    }

    // if switch is in a curve how do I calculate this?? A: a^2 = b^2 + c^3 - 2bc*cos(A)
    int b = getLaneRadius(lane);
    int c = b + laneDist;
    Double angleA = getAngle();
    Double a2 = Math.pow(b, 2) + Math.pow(c, 2) - (2 * b * c * Math.cos(Math.toRadians(angleA)));

    Double a = Math.sqrt(a2);

    return a;
  }

  /**
   * Calculates the maximum speed in the curve. Returns null if this is a straight piece
   * 
   * @param accidents
   * 
   * @return
   */
  public Double getMaximumSpeed(int laneIndex, AccidentInfo accidents) throws Exception
  {
    if (isStreight())
      return null;

    int r = getLaneRadius(laneIndex);

    Double sqrt = physics.getMaximumSpeed(r);

    // double sqrt = Math.sqrt(r * physics.getCENTRIPETAL_ACCELERATION());

    // if (getCrashesInthisCurve() != 0)
    // Double adjustedSpeed sqrt * (Math.pow(0.9, getCrashesInthisCurve()));

    Double adjustedSpeed = sqrt;
    if (accidents.isDangerousCurve(getCurveType()))
      adjustedSpeed = adjustedSpeed * (Math.pow(0.9, getCurveType().crashes));

    return adjustedSpeed;
  }

  public Double getMaxSpeedOfNextCurvePiece(int currentLaneIndex, Integer laneIndex) throws Exception {
    if (maxSpeedOfNextCurvePiece == null)
    {
      if (laneIndex == null)
      {
        // Determine lane index if possible
        if (isSwitchable())
        {
          if (isSwitchSent == TrackWrapper.LEFT)
          {
            laneIndex = currentLaneIndex - 1;
            currentLaneIndex = laneIndex;
          }
          else if (isSwitchSent == TrackWrapper.RIGHT)
          {
            laneIndex = currentLaneIndex + 1;
            currentLaneIndex = laneIndex;
          }
          else
          {
            // Lane change will not occur or has not been determined
          }
        }
      }
      if (isCurve())
      {
        // if laneIndex is still null, use inner lane
        if (laneIndex == null)
          laneIndex = getInnerLaneIndex();

        maxSpeedOfNextCurvePiece = track.getTrackMaxSpeed(pieceIndex, laneIndex, 0.0, Driver.DISTANCE_MAXSPEED_CALC);

        // maxSpeedOfNextCurvePiece = getMaximumSpeed(laneIndex);
      }
      else
      {
        maxSpeedOfNextCurvePiece = getNextPiece().getMaxSpeedOfNextCurvePiece(currentLaneIndex, laneIndex);
      }
    }
    return maxSpeedOfNextCurvePiece;
  }

  /**
   * Get the distance from the start of this piece to the start of the next switchable piece
   * 
   * @param laneIndex
   * @return
   */
  public Double getDistanceToNextSwitchPiece(Integer laneIndex) throws Exception {

    if (isSwitchable())
      return 0.0;

    return getLaneLenght(laneIndex) + getNextPiece().getDistanceToNextSwitchPiece(laneIndex);
  }

  public int getCrashesInthisCurve() throws Exception {
    return crashesInthisCurve;
  }

  public void setCrashesInthisCurveBackwards(Double leftLength) throws Exception {

    Double myLength = getLaneLenght(0); // Approximate length is fine therefore just use lane 0
    double left = leftLength - myLength;
    if (left > 0)
    {
      getPrevPiece().setCrashesInthisCurveBackwards(left);
    }
    this.crashesInthisCurve++;
  }

  public void setCrashesInthisCurveForwards(Double leftLength) throws Exception {

    Double myLength = getLaneLenght(0); // Approximate length is fine therefore just use lane 0
    double left = leftLength - myLength;
    if (left > 0)
    {
      getNextPiece().setCrashesInthisCurveForwards(left);
    }
    this.crashesInthisCurve++;
  }

  public CurveType getCurveTypeBackwards(CurveType lasttype) throws Exception
  {
    if (!isCurve())
      return null;

    int dir = getInnerLaneSide();
    Integer radius = getRadius();
    Double curveLength = getCurvePieceLength();

    CurveType tempType = new CurveType(radius, curveLength, dir);

    if (lasttype == null || lasttype.dir == dir && lasttype.radius == radius)
    {

      // Look for curve pieces of the same radius and direction in order to put togeather one curve type
      CurveType prevtype = getPrevPiece().getCurveTypeBackwards(tempType);
      if (prevtype != null)
      {
        // Add the length to the total length of the whole curve
        tempType.length = tempType.length + prevtype.length;
      }
    }

    // Now we should have identified a curve with constant radius and direction.
    // The length is compiled togeather when the recursion is done

    return tempType;
  }

  public CurveType getCurveTypeForwards(CurveType lasttype) throws Exception
  {
    if (!isCurve())
      return null;

    int dir = getInnerLaneSide();
    Integer radius = getRadius();
    Double curveLength = getCurvePieceLength();

    CurveType tempType = new CurveType(radius, curveLength, dir);

    if (lasttype == null || lasttype.dir == dir && lasttype.radius == radius)
    {

      // Look for curve pieces of the same radius and direction in order to put togeather one curve type
      CurveType nexttype = getNextPiece().getCurveTypeForwards(tempType);
      if (nexttype != null)
      {
        // Add the length to the total length of the whole curve
        tempType.length = tempType.length + nexttype.length;
      }
    }

    // Now we should have identified a curve with constant radius and direction.
    // The length is compiled togeather when the recursion is done

    return tempType;
  }

  private Double getCurvePieceLength() throws Exception {

    if (isStreight())
      return 0.0;
    return 2 * Math.PI * getRadius() * (Math.abs(getAngle()) / 360);
  }

  public CurveType getCurveType() throws Exception
  {

    if (isStreight())
      return null;

    if (completeType == null)
    {
      CurveType typeForwards = getCurveTypeForwards(null);
      CurveType typeBackwards = getCurveTypeBackwards(null);

      if (typeForwards.radius == typeBackwards.radius && typeForwards.dir == typeBackwards.dir)
      {
        double totalLength = typeForwards.length + typeBackwards.length - getCurvePieceLength(); // minus my length since it was added twice
                                                                                                 // in
                                                                                           // the above methods
        completeType = new CurveType(typeForwards.radius, totalLength, typeForwards.dir);
      }
      else
        completeType = typeBackwards;

      // Now we should have identified a complete curve with constant radius and direction.
      // The length is compiled togeather when the recursion is done
    }

    return completeType;
  }

}

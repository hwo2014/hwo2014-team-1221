package noobbot.wrappers;

import java.util.ArrayList;
import java.util.List;

import noobbot.hwo.crash.Crash;

import com.google.gson.Gson;

public class CrashWrapper {

  private List<Crash> kolarit = new ArrayList<Crash>();

  private static CrashWrapper instance;

  private Gson gson = new Gson();

  public static CrashWrapper getInstance()
  {
    if (instance == null)
    {
      instance = new CrashWrapper();
    }
    return instance;
  }

  public Crash addCrash(String json) throws Exception
  {
    Crash crash = gson.fromJson(json, Crash.class);
    kolarit.add(crash);

    return crash;
  }

  public Crash getLastCrash() throws Exception
  {
    return kolarit.get(kolarit.size() - 1);
  }

}

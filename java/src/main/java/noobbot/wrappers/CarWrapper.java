package noobbot.wrappers;

import noobbot.hwo.carpositions.Datum;
import noobbot.hwo.carpositions.Lane;

public class CarWrapper {

  public TrackWrapper getTrack() {
    return track;
  }

  private Datum carPositionInfo;
  private TrackWrapper track;
  private Double recordedThrottle;
  private String recordedAction;
  private Double recordedSpeed;

  public CarWrapper(Datum info, TrackWrapper trackWrapper)
  {
    this.carPositionInfo = info;
    this.track = trackWrapper;
  }

  public String getColor() throws Exception
  {
    return carPositionInfo.getId().getColor();
  }

  public String getName() throws Exception
  {
    return carPositionInfo.getId().getName();
  }

  public Double getAngle() throws Exception
  {
    return carPositionInfo.getAngle();
  }

  public Lane getLane() throws Exception
  {
    return carPositionInfo.getPiecePosition().getLane();
  }

  public Integer getLap() throws Exception
  {
    return carPositionInfo.getPiecePosition().getLap();
  }

  public Integer getPieceIndex() throws Exception
  {
    return carPositionInfo.getPiecePosition().getPieceIndex();
  }

  public Double getInPieceDistance() throws Exception
  {
    return carPositionInfo.getPiecePosition().getInPieceDistance();
  }

  public PieceWrapper getPiece() throws Exception {
    return track.getPiece(getPieceIndex());
  }

  // public boolean isOnInnerlane() {
  //
  // int lanenumber = getLane().getEndLaneIndex();
  // if (getPiece().getInnerLaneSide() == TrackWrapper.LEFT)
  // {
  // // lane number 0 is the leftmost lane
  // int leftMostLane = 0;
  // return lanenumber == leftMostLane;
  // }
  // else // if (getPiece().getInnerLaneSide() == TrackWrapper.RIGHT)
  // {
  // int rightMostLane = track.getRightMostLane();
  // return lanenumber == rightMostLane;
  // }
  // }

  /**
   * Check if the car will be on inner lane on the next piece
   * 
   * @return
   */
  public boolean isNextOnInnerlane() throws Exception {

    int lanenumber = getLane().getEndLaneIndex();
    if (getPiece().getNextPiece().getInnerLaneSide() == TrackWrapper.LEFT)
    {
      // lane number 0 is the leftmost lane
      int leftMostLane = 0;
      return lanenumber == leftMostLane;
    }
    else // if (getPiece().getInnerLaneSide() == TrackWrapper.RIGHT)
    {
      int rightMostLane = track.getRightMostLaneIndex();
      return lanenumber == rightMostLane;
    }
  }

  /**
   * Move towards the inner lane considering the following peace
   * 
   * @return
   */
  public int moveTowardsInnerlane() throws Exception {
    return getPiece().getNextPiece().getInnerLaneSide();
  }

  public Double getRecordedThrottle() throws Exception {
    return recordedThrottle;
  }

  public void setRecordedThrottle(Double currentThrottle) throws Exception {
    this.recordedThrottle = currentThrottle;
  }

  public Double getRecordedSpeed() throws Exception {
    return recordedSpeed;
  }

  public void setRecordedSpeed(Double currentSpeed) throws Exception {
    this.recordedSpeed = currentSpeed;
  }

  public String getRecordedAction() throws Exception {
    return recordedAction;
  }

  public void setRecordedAction(String recordedAction) throws Exception {
    this.recordedAction = recordedAction;
  }

  public Double getDistanceTo(CarWrapper car) throws Exception {

    Double myCarInpieceDistance = getInPieceDistance();
    Double carInPieceDistance = car.getInPieceDistance();

    int startIndex = getPieceIndex();
    int endIndex = car.getPieceIndex();

    int laneIndex = getLane().getEndLaneIndex();

    Double distance = track.getDistanceFromStartOfPieceToStartOfPiece(startIndex, endIndex, laneIndex) + carInPieceDistance
        - myCarInpieceDistance;
    return distance;
  }

}

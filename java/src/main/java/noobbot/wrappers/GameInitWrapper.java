package noobbot.wrappers;

import java.util.List;

import noobbot.ai.Physics;
import noobbot.hwo.gameinit.Car;
import noobbot.hwo.gameinit.GameInit;

import com.google.gson.Gson;

public class GameInitWrapper {

  GameInit gameInit;
  final Gson gson = new Gson();

  private List<Car> cars;
  private RaceSessionWrapper raceSession;
  private TrackWrapper track;
  private AccidentInfo accidents = new AccidentInfo();
  public Physics physics = new Physics();

  private static GameInitWrapper instance;

  private GameInitWrapper()
  {

  }

  public static GameInitWrapper getInstance()
  {
    if (instance == null)
    {
      instance = new GameInitWrapper();
    }

    return instance;
  }

  public void fromJson(String json) throws Exception
  {
    gameInit = gson.fromJson(json, GameInit.class);
    cars = gameInit.getData().getRace().getCars();
    raceSession = new RaceSessionWrapper(gameInit.getData().getRace().getRaceSession());
    track = new TrackWrapper(gameInit.getData().getRace().getTrack(), this.accidents, physics);
  }

  public String getMsgType() throws Exception
  {
    return gameInit.getMsgType();
  }

  public CarDimensionWrapper getCarDimensions(String carName) throws Exception
  {
    for (Car car: cars)
    {
      if (car.getId().getName().equals(carName))
        return new CarDimensionWrapper(car.getId().getName(), car.getDimensions().getLength(), car.getDimensions().getWidth());
    }
    return null;
  }

  public List<Car> getCars() throws Exception
  {
    return cars;
  }

  public TrackWrapper getTrack() throws Exception
  {
    return track;
  }

  public RaceSessionWrapper getRaceSession() throws Exception
  {
    return raceSession;
  }

}

package noobbot.wrappers;

import java.util.ArrayList;
import java.util.List;

public class AccidentInfo {

  private List<CurveType> accidentsInCurves = new ArrayList<CurveType>();

  public void addCrash(CurveType type)
  {
    CurveType foundtype = findCrash(type);
    if (foundtype == null)
    {
      accidentsInCurves.add(type);
      foundtype = type;
    }

    foundtype.crashes++;

  }

  public boolean isDangerousCurve(CurveType curveType) {

    CurveType crashedCurve = findCrash(curveType);

    if (crashedCurve != null)
    {
      curveType.crashes = crashedCurve.crashes; // make sure the number of crashes is up to date
      return true;
    }

    return false;
  }

  private CurveType findCrash(CurveType curveType) {
    for (CurveType type: accidentsInCurves)
    {
      if (type.length >= curveType.length && type.radius == curveType.radius)
      {
        return type;
      }
    }
    return null;
  }


}

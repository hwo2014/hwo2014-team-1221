package noobbot.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractListModel;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingWorker;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import noobbot.Main;
import noobbot.constants.RaceType;
import noobbot.logging.DashCauge;
import noobbot.logging.LogLevel;
import noobbot.logging.RaceInfo;

public class HelloWorldOpenFrame extends JFrame {

  private JFrame f = this;

  private JPanel contentPane;

  private Main main;

  private JTextArea mainTextArea;
  private JTextArea resultTextArea;
  private JTextArea progressTextArea;
  private JTextArea messageTextArea;
  private JTextArea angleInfoTextArea;
  private JTextArea aiTextArea;
  private JTextArea raceInfoTextArea;

  private RaceType raceType;
  private JTextField carCountTextField;
  private JTextField txtBotName;
  private JTextField statusTextField;

  private HelloWorldOpenFrame gui;

  private JCheckBox chckbxCrippeled;
  private JTextField limitTextField;

  public boolean shouldStopRace = false;

  private JSpinner spinnerLaps;
  private JTextField speedTextField;

  private JTextField txtPassword;
  private JTextField lapTextField;
  private JTextField posTextField;
  private JTextField dragTextField;
  private JTextField frictionConstTextField;
  private JTextField frictionCoeficientTextField;
  private JTextField throttleTextField;
  private JTextField angleTextField;
  private JTextField textField;

  public static void openFrame() {

    EventQueue.invokeLater(new Runnable() {
      public void run() {
        try {
          HelloWorldOpenFrame frame = new HelloWorldOpenFrame();
          frame.setVisible(true);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    });
  }

  /**
   * Create the frame.
   */
  public HelloWorldOpenFrame() {

    this.gui = this;

    setLocation(new Point(1, 1));
    setMinimumSize(new Dimension(1200, 500));
    setTitle("Team RUUKKI - Hello World Open");
    
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setBounds(100, 100, 450, 300);
    contentPane = new JPanel();
    contentPane.setMaximumSize(new Dimension(50000, 32767));
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setContentPane(contentPane);
    contentPane.setLayout(new BorderLayout(0, 0));
    
    JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
    contentPane.add(tabbedPane, BorderLayout.CENTER);

    mainTextArea = new JTextArea();

    JScrollPane scrollPane = new JScrollPane(mainTextArea);
    tabbedPane.addTab("Main", null, scrollPane, null);

    resultTextArea = new JTextArea();

    JScrollPane scrollPane_1 = new JScrollPane(resultTextArea);
    tabbedPane.addTab("Results", null, scrollPane_1, null);

    progressTextArea = new JTextArea();

    JScrollPane scrollPane_2 = new JScrollPane(progressTextArea);
    tabbedPane.addTab("Progress", null, scrollPane_2, null);

    messageTextArea = new JTextArea();

    JScrollPane scrollPane_3 = new JScrollPane(messageTextArea);
    tabbedPane.addTab("Messages", null, scrollPane_3, null);

    angleInfoTextArea = new JTextArea();
    angleInfoTextArea.setText("TICK, SPEED, MAXSPEED, (RADIUS), ANGLE, ANGLEDELTA, (FRICTION)");

    JScrollPane scrollPane_4 = new JScrollPane(angleInfoTextArea);
    tabbedPane.addTab("AngleInfo", null, scrollPane_4, null);

    aiTextArea = new JTextArea();

    JScrollPane scrollPane_5 = new JScrollPane(aiTextArea);
    tabbedPane.addTab("AI", null, scrollPane_5, null);

    raceInfoTextArea = new JTextArea();

    JScrollPane scrollPane_6 = new JScrollPane(raceInfoTextArea);
    tabbedPane.addTab("Race info", null, scrollPane_6, null);

    JPanel startPanel = new JPanel();
    contentPane.add(startPanel, BorderLayout.WEST);
    startPanel.setLayout(new BoxLayout(startPanel, BoxLayout.Y_AXIS));

    JPanel inputPanel = new JPanel();
    inputPanel.setAlignmentY(Component.TOP_ALIGNMENT);
    inputPanel.setBorder(new LineBorder(new Color(0, 0, 0)));
    startPanel.add(inputPanel);
    inputPanel.setLayout(new BoxLayout(inputPanel, BoxLayout.X_AXIS));
    
    JPanel panel = new JPanel();
    inputPanel.add(panel);
    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

    final JList serverList = new JList();
    serverList.setModel(new AbstractListModel() {
      String[] values = new String[] { "prost", "testserver", "hakkinen", "senna", "webber" };

      public int getSize() {
        return values.length;
      }

      public Object getElementAt(int index) {
        return values[index];
      }
    });
    serverList.setSelectedIndex(0);
    inputPanel.add(serverList);

    final JList trackList = new JList();
    trackList.setBorder(new LineBorder(new Color(0, 0, 0)));
    inputPanel.add(trackList);
    trackList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    trackList.setModel(new AbstractListModel() {
      String[] values = new String[] { "keimola", "germany", "usa", "france", "elaeintarha", "imola", "england", "suzuka", "pentag" };

      public int getSize() {
        return values.length;
      }

      public Object getElementAt(int index) {
        return values[index];
      }
    });
    trackList.setSelectedIndex(0);

    JPanel panel_2 = new JPanel();
    inputPanel.add(panel_2);
    panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.Y_AXIS));

    JPanel carPanel = new JPanel();
    carPanel.setAlignmentY(Component.TOP_ALIGNMENT);
    carPanel.setAlignmentX(Component.RIGHT_ALIGNMENT);
    panel_2.add(carPanel);
    carPanel.setLayout(new GridLayout(0, 2, 0, 0));

    JLabel lblBotName = new JLabel(" bot name");
    carPanel.add(lblBotName);

    txtBotName = new JTextField();
    carPanel.add(txtBotName);
    txtBotName.setText("Ruukki");
    txtBotName.setColumns(10);

    JLabel lblLaps = new JLabel(" laps");
    carPanel.add(lblLaps);

    spinnerLaps = new JSpinner();
    carPanel.add(spinnerLaps);
    spinnerLaps.setModel(new SpinnerNumberModel(new Integer(3), new Integer(0), null, new Integer(1)));

    JLabel lblcars = new JLabel(" #Cars");
    carPanel.add(lblcars);

    carCountTextField = new JTextField();
    carPanel.add(carCountTextField);
    carCountTextField.setText("1");
    carCountTextField.setColumns(10);

    JLabel lblPassword = new JLabel(" Password");
    carPanel.add(lblPassword);

    txtPassword = new JTextField();
    txtPassword.setText("");
    carPanel.add(txtPassword);
    txtPassword.setColumns(10);

    chckbxCrippeled = new JCheckBox("crippeled");
    carPanel.add(chckbxCrippeled);

    limitTextField = new JTextField();
    carPanel.add(limitTextField);
    limitTextField.setText("0.9");
    limitTextField.setColumns(10);

    JPanel dashPanel = new JPanel();
    startPanel.add(dashPanel);
    dashPanel.setLayout(new BoxLayout(dashPanel, BoxLayout.Y_AXIS));

    JPanel buttonPanel = new JPanel();
    dashPanel.add(buttonPanel);

    JButton btnStartRace = new JButton("Start");
    buttonPanel.add(btnStartRace);

    JButton btnJoinRace = new JButton("Join");
    buttonPanel.add(btnJoinRace);

    JButton btnStop = new JButton("Stop");
    btnStop.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {
        shouldStopRace = true;
      }
    });
    buttonPanel.add(btnStop);
    btnJoinRace.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent arg0) {
        String txt = statusTextField.getText();
        statusTextField.setText(txt + "Requested Join\n");
        raceType = RaceType.JOINRACE;
        clearWindows();
        StartRaceWorker worker = new StartRaceWorker();
        worker.setFields(carCountTextField, trackList, txtBotName, serverList, chckbxCrippeled, limitTextField, txtPassword);
        worker.execute();
      }

      private void clearWindows() {

        shouldStopRace = false;
        mainTextArea.setText("");
        resultTextArea.setText("");
        progressTextArea.setText("");
        messageTextArea.setText("");
        angleInfoTextArea.setText("");
        aiTextArea.setText("");
        raceInfoTextArea.setText("");

      }
    });
    btnStartRace.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {
        String txt = statusTextField.getText();
        statusTextField.setText(txt + "Requested Start:" + "\n");
        raceType = RaceType.JOINSINGLE;
        StartRaceWorker worker = new StartRaceWorker();
        worker.setFields(carCountTextField, trackList, txtBotName, serverList, chckbxCrippeled, limitTextField, txtPassword);
        worker.execute();
      }
    });

    JPanel caugePanel = new JPanel();
    dashPanel.add(caugePanel);
    caugePanel.setLayout(new GridLayout(0, 3, 0, 0));

    JLabel lblThrottle = new JLabel("throttle");
    caugePanel.add(lblThrottle);

    JLabel lblSpeedmax = new JLabel("speed/max");
    caugePanel.add(lblSpeedmax);

    JLabel lblAngle = new JLabel("angle");
    caugePanel.add(lblAngle);

    throttleTextField = new JTextField();
    throttleTextField.setEditable(false);
    caugePanel.add(throttleTextField);
    throttleTextField.setColumns(10);

    speedTextField = new JTextField();
    speedTextField.setEditable(false);
    caugePanel.add(speedTextField);
    speedTextField.setColumns(10);

    angleTextField = new JTextField();
    angleTextField.setEditable(false);
    caugePanel.add(angleTextField);
    angleTextField.setColumns(10);

    JLabel lblPosition = new JLabel("position");
    caugePanel.add(lblPosition);

    JLabel lblPiecelap = new JLabel("piece/lap");
    caugePanel.add(lblPiecelap);

    JLabel lblNewLabel = new JLabel("New label");
    caugePanel.add(lblNewLabel);

    posTextField = new JTextField();
    posTextField.setEditable(false);
    caugePanel.add(posTextField);
    posTextField.setColumns(10);

    lapTextField = new JTextField();
    lapTextField.setEditable(false);
    caugePanel.add(lapTextField);
    lapTextField.setColumns(10);

    textField = new JTextField();
    caugePanel.add(textField);
    textField.setColumns(10);

    JLabel lblDragc = new JLabel("dragConst");
    caugePanel.add(lblDragc);

    JLabel lblFrictionc = new JLabel("frictionConst");
    caugePanel.add(lblFrictionc);

    JLabel lblFrictioncoeficient = new JLabel("frictionCoeficient");
    caugePanel.add(lblFrictioncoeficient);

    dragTextField = new JTextField();
    dragTextField.setEditable(false);
    caugePanel.add(dragTextField);
    dragTextField.setColumns(10);

    frictionConstTextField = new JTextField();
    frictionConstTextField.setEditable(false);
    caugePanel.add(frictionConstTextField);
    frictionConstTextField.setColumns(10);

    frictionCoeficientTextField = new JTextField();
    frictionCoeficientTextField.setEditable(false);
    caugePanel.add(frictionCoeficientTextField);
    frictionCoeficientTextField.setColumns(10);

    statusTextField = new JTextField();
    dashPanel.add(statusTextField);
    statusTextField.setColumns(10);
  }

  public void log(LogLevel level, String msg, RaceInfo info) {

    if (msg == null)
    {
      return;
    }

    switch (level)
    {
      case CURVEANGLE: {
        String txt = angleInfoTextArea.getText();
        angleInfoTextArea.setText(txt + "\n" + msg);
        break;
      }
      case ACTIONRULE_USED: {
        String txt = mainTextArea.getText();
        mainTextArea.setText(txt + "\n" + msg);
        break;
      }
      case GAMERESULTS: {
        String txt = resultTextArea.getText();
        resultTextArea.setText(txt + "\n" + msg);
        // continue to PROGRESS
      }
      case PROGRESS: {
        String txt = progressTextArea.getText();
        progressTextArea.setText(txt + "\n" + msg);
        break;
      }
      case CARPOS_MESSAGE: {
        // do not log. Look for file instead
        break;
      }
      case MESSAGE: {
        String txt = messageTextArea.getText();
        messageTextArea.setText(txt + "\n" + msg);
        break;
      }
      case AI: {
        String txt = aiTextArea.getText();
        aiTextArea.setText(txt + "\n" + msg);
        break;
      }
      case CRASH: {
        String txt = statusTextField.getText();
        statusTextField.setText(txt + "\n" + msg);
        break;
      }
      case CARPOSITIONS: {
        String txt = raceInfoTextArea.getText();
        raceInfoTextArea.setText(txt + "\n" + msg);
        break;
      }
      default: {
        break;
      }
    }
  }

  public void logDashInfo(DashCauge cauge, String msg) {

    if (msg == null)
    {
      return;
    }

    switch (cauge)
    {
      case SPEED: {
        speedTextField.setText(msg);
        break;
      }
      case THROTTLE: {
        throttleTextField.setText(msg);
        break;
      }
      case ANGLE: {
        angleTextField.setText(msg);
        break;
      }
      case LAP: {
        lapTextField.setText(msg);
        break;
      }
      case POS: {
        posTextField.setText(msg);
        break;
      }
      case FRICTCOEF: {
        frictionCoeficientTextField.setText(msg);
        break;
      }
      case FRICTCONST: {
        frictionConstTextField.setText(msg);
        break;
      }
      case DRAG: {
        dragTextField.setText(msg);
        break;
      }
      default: {
        break;
      }
    }

  }

  class StartRaceWorker extends SwingWorker<String, Integer>
  {
    private JTextField carCountTextField;
    private JList<String> trackList;
    private JTextField txtBotName;
    private JList listHostName;
    private JTextField txtLimit;
    private JTextField txtPassword;

    public void setFields(JTextField carCountTextField,
                          JList trackList,
                          JTextField txtBotName,
                          JList serverList,
                          JCheckBox chckbxCrippeled,
                          JTextField limitTextField,
                          JTextField txtPassword) {
      this.carCountTextField = carCountTextField;
      this.trackList = trackList;
      this.txtBotName = txtBotName;
      this.listHostName = serverList;
      this.txtLimit = limitTextField;
      this.txtPassword = txtPassword;
    }

    protected String doInBackground() throws Exception
    {
      String host = listHostName.getSelectedValue().toString() + ".helloworldopen.com";
      switch (raceType)
      {
        case JOINSINGLE: {
          main = Main.connectToTestServer(host, 8091);
          main.gui = gui;
          main.startRace(host, false);
          break;
        }
        case JOINRACE: {
          Integer count = Integer.parseInt(this.carCountTextField.getText());
          String track = (String)this.trackList.getSelectedValue();
          String psw = (String)this.txtPassword.getText();
          if (psw.equals(""))
            psw = null;
          main = Main.connectToTestServer(host, 8091);
          main.gui = gui;
          RaceInfo raceInfo = new RaceInfo(host, track, count, txtBotName.getText(), chckbxCrippeled.isSelected(),
              Double.parseDouble(txtLimit.getText()), psw);
          main.joinRace(raceInfo);
          break;
        }
        case STARTRACE:
          break;
        default:
          break;
      }
      return "Race ended";
    }

    protected void done()
    {
      try
      {
        JOptionPane.showMessageDialog(f, get());
      } catch (Exception e)
      {
        e.printStackTrace();
      }
    }
  }

  public int getLaps() {
    return (int)spinnerLaps.getValue();
  }

}

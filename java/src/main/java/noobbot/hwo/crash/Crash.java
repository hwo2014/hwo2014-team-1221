
package noobbot.hwo.crash;

import javax.annotation.Generated;

import com.google.gson.annotations.Expose;

@Generated("org.jsonschema2pojo")
public class Crash {

    @Expose
    private String msgType;
    @Expose
    private Data data;
    @Expose
    private String gameId;
    @Expose
    private Integer gameTick;

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public Crash withMsgType(String msgType) {
        this.msgType = msgType;
        return this;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Crash withData(Data data) {
        this.data = data;
        return this;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public Crash withGameId(String gameId) {
        this.gameId = gameId;
        return this;
    }

    public Integer getGameTick() {
        return gameTick;
    }

    public void setGameTick(Integer gameTick) {
        this.gameTick = gameTick;
    }

    public Crash withGameTick(Integer gameTick) {
        this.gameTick = gameTick;
        return this;
    }

}

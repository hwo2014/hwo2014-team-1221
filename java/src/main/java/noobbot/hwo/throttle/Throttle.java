
package noobbot.hwo.throttle;

import javax.annotation.Generated;

import com.google.gson.annotations.Expose;

@Generated("org.jsonschema2pojo")
public class Throttle {

    @Expose
    private String msgType;
    @Expose
    private Double data;
    @Expose
    private Integer gameTick;

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public Double getData() {
        return data;
    }

    public void setData(Double data) {
        this.data = data;
    }

    public Integer getGameTick() {
        return gameTick;
    }

    public void setGameTick(Integer gameTick) {
        this.gameTick = gameTick;
    }

}

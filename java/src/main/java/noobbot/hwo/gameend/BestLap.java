
package noobbot.hwo.gameend;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("org.jsonschema2pojo")
public class BestLap {

    @Expose
    private Car_ car;
    @Expose
    private Result__ result;

    public Car_ getCar() {
        return car;
    }

    public void setCar(Car_ car) {
        this.car = car;
    }

    public Result__ getResult() {
        return result;
    }

    public void setResult(Result__ result) {
        this.result = result;
    }

}
